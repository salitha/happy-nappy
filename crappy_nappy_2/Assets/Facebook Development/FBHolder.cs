﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using Facebook.Unity;
using Facebook.MiniJSON;


public class FBHolder : MonoBehaviour {
	public string fbPlayerID;
	public int highscore;
	public Text highscoreText;

	private string m_AccessToken;
	private string m_userID;

	private int m_userFbScore;

	public GameObject UIFBisLoggedIn;
	public GameObject UIFBnotLoggedIn;
	public GameObject UIFBAvatar;
	public GameObject UIFBUserName;
	public GameObject UIFBUserScore;
	public GameObject UIFBUserImage;
	public GameObject UIInputText;
	public GameObject UIRankText;

	public Text scoresDebug;
	private List<object> scoresList = null;

	private Dictionary<string,string> profile = null;
	private Dictionary<string,string> FriendsScore = null;

	public GameObject ScoreEntryPanel;

	public GameObject ScoreScrolList;

	void Awake(){
		//PlayerPrefs.DeleteAll();
		//PlayerPrefs.SetInt("score",0);
		Debug.Log ("Previous High Score : " + PlayerPrefs.GetInt ("score", 0));
		//PlayerPrefs.SetInt ("GameHighScore", 0);

		float x;
		float y;
		x = Screen.width * 0.7f; 
		y = x * 0.12f;

		print("X val = " + x);
		print("Y val = " + y);
		ScoreScrolList.GetComponent<GridLayoutGroup>().cellSize = new Vector2(x,y);
	}



	private void SetInit(){


		Debug.Log("FB Init done.");

		if(FB.IsLoggedIn)
		{
			DealWithFbMenues(true);
			Debug.Log("FB Logged In");

		}else{
			DealWithFbMenues(false);
		}
	}

	private void OnHideUnity(bool isGameShown){


		if(!isGameShown)
		{
			Time.timeScale = 0;

		}else{

			Time.timeScale = 1;
		}
	}



	public void FBlogin(){
		this.CallFBLogin();
	}


	public void Leaderboard()
	{

		if (FB.IsLoggedIn) {

			FB.Mobile.RefreshCurrentAccessToken (RefreshCallback);
			DealWithFbMenues (true);
			Debug.Log ("FB Logged In");
		}else{

			DealWithFbMenues(false);
		}

	}

	private void CallFBLogin()
	{
		FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, this.AuthCallback);
	}

	void AuthCallback(IResult result){
		if (result.Error != null) {

			UIFBisLoggedIn.SetActive (false);
			UIFBnotLoggedIn.SetActive(true);


		}

		if (FB.IsLoggedIn) {

			FB.Mobile.RefreshCurrentAccessToken (RefreshCallback);
			Debug.Log ("FB Login Worked");
			//PlayerPrefs.SetInt ("");


			if (PlayerPrefs.GetInt ("firstTimeLoadFB", 0) == 0) {
				Debug.Log ("firstTimeLoadFB");
				//PlayerPrefs.SetInt ("TotalCoins", PlayerPrefs.GetInt ("TotalCoins") + 5000);
				PlayerPrefs.SetInt ("firstTimeLoadFB", 1);
				//SetScore ();
			}

			DealWithFbMenues (true);

		} else {

			Debug.Log ("FB Login Failed");
			DealWithFbMenues (false);
		}
	}

	void DealWithFbMenues(bool isLoggedIn){


		if(isLoggedIn)
		{
			UIFBisLoggedIn.SetActive (true);
			UIFBnotLoggedIn.SetActive(false);

			StartCoroutine (UserScore ());

		}else{
			FB.Init(this.SetInit, this.OnHideUnity);
			UIFBisLoggedIn.SetActive (false);
			UIFBnotLoggedIn.SetActive(true);

		}
	}

	private string shareLink = "https://developers.facebook.com/";
	private string shareTitle = "Link Title";
	private string shareDescription = "Link Description";
	private string shareImage = "http://i.imgur.com/j4M7vCO.jpg";
	string dbgStr;

	void HandleResult(IResult result)
	{
		if (result == null)
		{
			dbgStr= "Null Response";
			return;
		}


		if (!string.IsNullOrEmpty(result.Error))
		{
			dbgStr= "Error Response:" + result.Error;
		}
		else if (result.Cancelled)
		{
			dbgStr= "Cancelled Response:" + result.RawResult;
		}
		else if (!string.IsNullOrEmpty(result.RawResult))
		{
			dbgStr= "Success Response:" + result.RawResult;
		}
		else
		{
			dbgStr="Empty Response";
		}

	}

	public void Sharelink(){

		FB.ShareLink (
			new System.Uri (shareLink),
			shareTitle,
			shareDescription,
			new System.Uri (shareImage),
			HandleResult);
	}

	public void UpdateLeaderBoard(){

		int m_CurrentHighScore = 0;
		for(int LevelNo = 1; LevelNo < 8; LevelNo++)
		{
			for(int StageNo = 0; StageNo < 4; StageNo++)
			{
				m_CurrentHighScore += PlayerPrefs.GetInt(LevelNo+""+StageNo, 0);
			}
		}
		#if UNITY_EDITOR
		Debug.Log ("Current High Score : " + m_CurrentHighScore);
		Debug.Log ("Current Game High Score : " + PlayerPrefs.GetInt ("GameHighScore", 0));
		#endif


		if (m_CurrentHighScore > PlayerPrefs.GetInt ("GameHighScore", 0)) {
			PlayerPrefs.SetInt ("GameHighScore", m_CurrentHighScore);

			#if UNITY_EDITOR
			Debug.Log ("Game High Score  :  " + PlayerPrefs.GetInt ("GameHighScore", 0));
			#endif

			if (m_userFbScore < m_CurrentHighScore) {
				#if UNITY_EDITOR
				Debug.Log ("Higscore reached");
				#endif
				string posthighscore = m_CurrentHighScore.ToString ();

				StartCoroutine (SubmitScore (posthighscore));

			}  
		}
		else
		{		
			StartCoroutine (GetScore ());
		}
	}

	public void GetScores(){
		//SetScore ();
		FB.Mobile.RefreshCurrentAccessToken (RefreshCallback);
		StartCoroutine (GetScoreRefresh ());
	

	}



	IEnumerator GetScoreRefresh(){

		WWW www = new WWW ("https://graph.facebook.com/v2.7/356341891368268/scores?limit=10&access_token=" + m_AccessToken);
		yield return www;

		if (!www.isDone || www.error != null) {

			scoresDebug.text = www.error;

		}else{
			var dict = Json.Deserialize(www.text) as Dictionary<string,object>;
			var friends = new List<object>();
			friends  = (List<object>)dict["data"];

			foreach(Transform child in ScoreScrolList.transform)
			{

				GameObject.Destroy(child.gameObject);

			}

			int count = 0;
			foreach (object score in friends) {

				count++;
				if (count == 11) {

					break;
				}

				var entry = (Dictionary<string,object>) score;
				var user = (Dictionary<string,object>) entry["user"];

				//scoresDebug.text = scoresDebug.text +"UN: "+ user["name"]+" - " + entry["score"]+" - "+"User ID : " +user["id"]+",";

				GameObject scorePanal;

				scorePanal = Instantiate(ScoreEntryPanel) as GameObject;
				scorePanal.transform.parent = ScoreScrolList.transform;


				Transform ThisScoreName = scorePanal.transform.Find("txtFreindName");
				Transform ThisScoreScore = scorePanal.transform.Find("txtFreindscore");
				Transform Rank = scorePanal.transform.Find ("txtRank");

				Text rank = Rank.GetComponent<Text> ();
				rank.text = (count ).ToString ();

				Text scoreName = ThisScoreName.GetComponent<Text>();
				Text ScoreScore = ThisScoreScore.GetComponent<Text>();

				scoreName.text = user["name"].ToString();
				ScoreScore.text =entry["score"].ToString();

				Transform TheUserAvatar = scorePanal.transform.Find("imgFriendAvatar");
				Image UserAvatar = TheUserAvatar.GetComponent<Image>();

				StartCoroutine (GetUserImages (user["id"].ToString(),UserAvatar));

			}
			StartCoroutine (UserImgScoreName ());
		}
	}

	IEnumerator GetScore()
	{
		
		WWW www = new WWW ("https://graph.facebook.com/v2.7/356341891368268/scores?limit=10&access_token=" + m_AccessToken);
		yield return www;
		if (!www.isDone) {

			StartCoroutine (GetScoreRefresh ());

		}
		var dict = Json.Deserialize(www.text) as Dictionary<string,object>;

		var friends = new List<object>();
		friends  = (List<object>)dict["data"];

		foreach(Transform child in ScoreScrolList.transform)
		{

			GameObject.Destroy(child.gameObject);

		}

		int count = 0;

		foreach (object score in friends) {

			count++;
			Debug.Log ("Aiyoooo count" + count);
			if (count == 11) {

				break;

			}


			var entry = (Dictionary<string,object>) score;
			var user = (Dictionary<string,object>) entry["user"];


			GameObject scorePanal;

			scorePanal = Instantiate(ScoreEntryPanel) as GameObject;

			scorePanal.transform.parent = ScoreScrolList.transform;


			Transform ThisScoreName = scorePanal.transform.Find("txtFreindName");
			Transform ThisScoreScore = scorePanal.transform.Find("txtFreindscore");
			Transform Rank = scorePanal.transform.Find ("txtRank");

			Text rank = Rank.GetComponent<Text> ();
			rank.text = (count ).ToString ();

			Text scoreName = ThisScoreName.GetComponent<Text>();
			Text ScoreScore = ThisScoreScore.GetComponent<Text>();

			scoreName.text = user["name"].ToString();
			ScoreScore.text =entry["score"].ToString();

			Transform TheUserAvatar = scorePanal.transform.Find("imgFriendAvatar");
			Image UserAvatar = TheUserAvatar.GetComponent<Image>();

			StartCoroutine (GetUserImages (user["id"].ToString(),UserAvatar));

		}
		StartCoroutine (UserImgScoreName ());

		print ("m_userFbScore = " + m_userFbScore);
//		highscore = PlayerPrefs.GetInt("score", -1);
//		if (m_userFbScore < 0) {
//			SetScore ();
//		}

	}

	// get user's images
	IEnumerator GetUserImages(string userid,Image UserImage)
	{

		WWW www = new WWW ("https://graph.facebook.com/" + userid + "/picture?type=small");

		yield return www;

		UserImage.sprite = Sprite.Create(www.texture,new Rect(0,0,www.texture.width,www.texture.height),new Vector2(0,0));

	}

	public void SetScore(){

		print(" <<  public void SetScore() ");
		highscore = PlayerPrefs.GetInt("score", -1);
		//PlayerPrefs.GetInt ("GameHighScore", -1)

		print("m_userFbScore = " + m_userFbScore.ToString());
		print("highscore = " + highscore.ToString());

		highscoreText.text = "BEST : " + highscore.ToString();
		if (m_userFbScore < highscore) 
		{
			FB.Mobile.RefreshCurrentAccessToken (RefreshCallback);
			StartCoroutine (SubmitScore (highscore.ToString ()));


		}else
		{		
			StartCoroutine (GetScore ());
		}

	}




	void RefreshCallback(IAccessTokenRefreshResult result) {
		if (FB.IsLoggedIn) {
			//Debug.Log (result.AccessToken.ToString());
			m_AccessToken = result.AccessToken.TokenString;//.ToString();
			m_userID = result.AccessToken.UserId;
			//retStr = result.AccessToken.TokenString;
			//scoresDebug.text = result.AccessToken.TokenString;
		}
	}



	IEnumerator SubmitScore(string UserScore){

		WWWForm form = new WWWForm();
		//form.headers["access_token"] = "EAAQWECKZBoZBsBACbocp6jj77DEiTFZBWvKZCi8KdaZBfDZBZBASrPe4HgQHYqKqFZB6OxFZCukGnQ0eGIfHLUbGpcFGsQTdLArE6YIXHGgb6AhTMRP7fXB3pg6AJWOiaiZCEqrq14bTdwveZA8lNimlbs71WNFbFWrwXqoC0qx8I2zBf6RleSAhD4W";
		//form.headers["score"]="6000";
		//form.AddField("access_token","EAAQWECKZBoZBsBACbocp6jj77DEiTFZBWvKZCi8KdaZBfDZBZBASrPe4HgQHYqKqFZB6OxFZCukGnQ0eGIfHLUbGpcFGsQTdLArE6YIXHGgb6AhTMRP7fXB3pg6AJWOiaiZCEqrq14bTdwveZA8lNimlbs71WNFbFWrwXqoC0qx8I2zBf6RleSAhD4W");
		form.AddField("access_token",m_AccessToken);
		////-----
		///string InputText = UIInputText.GetComponent<InputField> ().text;
		////------
		////form.AddField ("score", "900");
		form.AddField ("score",UserScore);
		////form.AddField ("score",InputText);
		WWW w = new WWW("https://graph.facebook.com/v2.7/me/scores", form);
		yield return w;
		//retStr = w.text;
		scoresDebug.text = w.text;
		StartCoroutine (GetScore ());
	}

	IEnumerator UserScore(){


		//WWW www = new WWW ("https://graph.facebook.com/v2.7/me/scores?access_token=" + m_AccessToken);
		WWW www = new WWW ("https://graph.facebook.com/v2.7/me/score?fields=application%2Cscore&access_token=" + m_AccessToken);
		yield return www;

		if (www.error != null || www.isDone == false) {

			if(FB.IsLoggedIn)
			{
				UIFBisLoggedIn.SetActive (true);
				UIFBnotLoggedIn.SetActive(false);

				//StartCoroutine (UserScore ());

			}else{
				//FB.Init(this.SetInit, this.OnHideUnity);
				UIFBisLoggedIn.SetActive (false);
				UIFBnotLoggedIn.SetActive(true);

			}

		}else{

			////string testS = "{\n   \"data\": [{ \"score\": 9000, \"user\": { \"name\": \"Testapp Crappy\", \"id\": \"100728150395047\" } }]}";
			var dict = Json.Deserialize (www.text) as Dictionary<string,object>;
			////var dict = Json.Deserialize(testS) as Dictionary<string,object>;

			var friends = new List<object> ();
			friends = (List<object>)dict ["data"];

			if (friends.Count > 0) {

				var entry = (Dictionary<string,object>)friends [0];

				//================
				for (int i = 0; i < friends.Count; i++) {

					entry = (Dictionary<string,object>)friends [i];
					var app = (Dictionary<string,object>)entry ["application"];

					if ((app ["id"].ToString ()) == "356341891368268")
						break;

				}
				//================

				var user = (Dictionary<string,object>)entry ["user"];

				string score = entry ["score"].ToString ();
				m_userFbScore = System.Int32.Parse (score);
				Debug.Log ("Yupeeeee1111..." + m_userFbScore);

//				UpdateLeaderBoard ();
				SetScore();
			} else {
				print("Else -> m_userFbScore = -1");
				m_userFbScore = -1;
				SetScore();
				//No data

			}
		}
	}

	IEnumerator UserImgScoreName(){


		//WWW www = new WWW ("https://graph.facebook.com/v2.7/me/scores?access_token="+m_AccessToken);
		WWW www = new WWW ("https://graph.facebook.com/v2.7/me/score?fields=application%2Cscore&access_token=" + m_AccessToken);

		yield return www;


		////string testS = "{\n   \"data\": [{ \"score\": 9000, \"user\": { \"name\": \"Testapp Crappy\", \"id\": \"100728150395047\" } }]}";
		var dict = Json.Deserialize(www.text) as Dictionary<string,object>;
		////var dict = Json.Deserialize(testS) as Dictionary<string,object>;

		var friends = new List<object>();
		friends  = (List<object>)dict["data"];

		if (friends.Count > 0) {

			var entry = (Dictionary<string,object>)friends [0];

			//================
			for (int i = 0; i < friends.Count; i++) {

				entry = (Dictionary<string,object>)friends [i];
				var app = (Dictionary<string,object>)entry ["application"];

				if ((app ["id"].ToString ()) == "356341891368268")
					break;

			}
			//================
			var user = (Dictionary<string,object>)entry ["user"];

			string score = entry ["score"].ToString();
			m_userFbScore = System.Int32.Parse (score);
			Debug.Log ("Yupeeeee1111..." + m_userFbScore);

			fbPlayerID = (string)user["id"].ToString();
			PlayerPrefs.SetString("playerID_Temp", fbPlayerID);

			scoresDebug.text = scoresDebug.text + "UN: " + user ["name"] + " - " + entry ["score"] + " - " + "User ID : " + user ["id"] + ",";
			Text Username = UIFBUserName.GetComponent<Text> ();
			Username.text = "Hi there  \n" + user ["name"];

			Text UserScore = UIFBUserScore.GetComponent<Text> ();
			UserScore.text = "Score : " + entry ["score"];

			Image UserImage = UIFBUserImage.GetComponent<Image> ();

			StartCoroutine (GetUserImage (user ["id"].ToString (), UserImage));
		} else {

		}
	}

	IEnumerator GetUserImage(string userid,Image UserImage){

		WWW www = new WWW ("https://graph.facebook.com/" + userid + "/picture?type=small");

		yield return www;

		UserImage.sprite = Sprite.Create(www.texture,new Rect(0,0,www.texture.width,www.texture.height),new Vector2(0,0));


	}
}
