﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class scare_meter_trigger : MonoBehaviour {


	public Image[] scare_meter;
	public int object_count;
	public int stay_count;
	public bool stay = false;

	public bool enter=false;
	public bool exit = false;

	public Animator scare_meter_fade;
	private Color prvColor;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{ 
		if (enter) 
		{
			scare_meter_fade.enabled = true;
			scare_meter_fade.SetBool ("fade", false);
			enter = false;





		}
		else if (!stay) 
		{
			scare_meter_fade.SetBool ("fade", true);
			exit = false;


		}

	}

	void FixedUpdate ()
	{
		if (stay)
		{
			stay_count++;
			if (stay_count == object_count) 
			{
				//print ("equla");
			}

			else 
			{
				//print ("stay stop");
				stay = false;
				enter = false;
				exit = true;
				object_count = 0;
				stay_count = 0;
//				scare_meter [0].color = new Color32 (255, 255, 255, 255);
//				scare_meter [1].color = new Color32 (255, 255, 255, 255);
//				scare_meter [2].color = new Color32 (255, 255, 255, 255);
//				scare_meter [3].color = new Color32 (255, 255, 255, 255);	
			}
		}

	}

	 void OnTriggerEnter(Collider other)
	{
		//print ("enter");
		enter = true;
		exit = false;
//		scare_meter [0].color= new Color32 (100, 100, 100, 255);
//		scare_meter [1].color =  new Color32 (100, 100, 100, 255);
//		scare_meter [2].color =  new Color32 (100, 100, 100, 255);
//		scare_meter [3].color =  new Color32 (100, 100, 100, 255);
		//StartCoroutine (color_change ());
	}

	void OnTriggerStay(Collider other)
	{

		stay = true;
		object_count++;
		//print ("onstay");

		if (other == null) 
		{
			print ("null");

		} 
		else
		{
//			scare_meter [0].color = new Color32 (100, 100, 100, 255);
//			scare_meter [1].color = new Color32 (100, 100, 100, 255);
//			scare_meter [2].color = new Color32 (100, 100, 100, 255);
//			scare_meter [3].color = new Color32 (100, 100, 100, 255);
		}
	}
	void OnTriggerExit(Collider other)
	{
		enter = false;
		exit = true;
//		print ("exit");
//		scare_meter [0].color = new Color32 (255, 255, 255, 255);
//		scare_meter [1].color = new Color32 (255, 255, 255, 255);
//		scare_meter [2].color = new Color32 (255, 255, 255, 255);
//		scare_meter [3].color = new Color32 (255, 255, 255, 255);
	}

//	private IEnumerator color_change()
//	{
//		yield return new WaitForSeconds (1f);
//		if (!exit) 
//		{
//			exit = false;
//			scare_meter [0].color = new Color32 (255, 255, 255, 255);
//			scare_meter [1].color = new Color32 (255, 255, 255, 255);
//			scare_meter [2].color = new Color32 (255, 255, 255, 255);
//			scare_meter [3].color = new Color32 (255, 255, 255, 255);
//		}
//	}

}
