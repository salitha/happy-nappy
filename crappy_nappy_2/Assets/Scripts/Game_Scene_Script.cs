﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Game_Scene_Script : MonoBehaviour {

	public GameObject pause_panel;
	public GameObject child_img;
	public GameObject background;

	public Sprite[] child_img_set;
	public Sprite[] background_set;

	public GameObject babyprefab;

	// Use this for initialization
	void Start ()
	{
		RandomImage ();
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.A))
		GamePause ();
	}

	public void Home_btn()
	{
		SceneManager.LoadScene ("crappy_nappy");
		//Application.LoadLevel (0);
	}

	public void Restart_btn()
	{
//		save_data.replay_btn_id = true;
//		SceneManager.LoadScene ("crappy_nappy");

		SceneManager.LoadScene ("Game");
	}

	public void play()
	{
		Time.timeScale = 1;
		pause_panel.SetActive (false);
		Debug.Log ("play");
	}


	public void GamePause()
	{
		Time.timeScale = 0;
		pause_panel.SetActive (true);
		Debug.Log ("pause");

	}

	public void Replay()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene ("Game");
		//Application.LoadLevel (1);
	}

	public void RandomImage()
	{
		babyprefab.GetComponentInChildren<Baby> ().babyCode = save_data.baby_id;
		if(save_data.back_id==0)
			background.gameObject.GetComponent<SpriteRenderer>().sprite = background_set[save_data.back_id];
		else
			background.gameObject.GetComponent<SpriteRenderer>().sprite = background_set[save_data.back_id-1];
		
	}


}
