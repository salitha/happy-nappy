﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class NewPool : MonoBehaviour {

	public static NewPool Instance{ set; get;}

	public Scare_Meter scare_meter_script;
	public Score_Manager score_manager_script;


	public Queue<GameObject> items = new Queue<GameObject> ();

	public GameObject ItemPrefab;

	public int numberOfObjects;

	private bool strat;
	public bool first_done=true;
	public	bool second_done=false;

	public bool active_combo=false;

	public Vector3[,] combos_pos=new Vector3[6,4];



	void Awake()
	{
		Instance = this;
	}

	void Start()
	{
		/*
		for (int v1 = 0; v1 < 6; v1++) {

			for (int v2 = 0; v2 < 4; v2++) {
				n.combos_pos [v1, v2] = Vector3.zero;
			}
		}*/







		strat = true;


		items = new Queue<GameObject>(numberOfObjects);

		for(int i = 0; i < numberOfObjects; i++){
			//ItemPrefab.GetComponent<Item_Script>().reset();
			items.Enqueue((GameObject)Instantiate(ItemPrefab));


		}

		StartCoroutine (ItemShooting());

		scare_meter_script = Scare_Meter.Instance;
		score_manager_script = Score_Manager.Instance;
		print(scare_meter_script.cur_Helth.ToString());
	}

	void Update()
	{
//		if(save_data.objectid == 4)
//		{
//			save_data.objectid = 0;
//		}


	}

	private Item_Script GetItems()
	{
		Item_Script v;

		if(items.Count == 0) {

			v= Instantiate (ItemPrefab).GetComponent<Item_Script>() ;
			//print ("if(items.Count == 0)");
		} 
		else {
			
			v=	(items.Dequeue ()).GetComponent<Item_Script>();
			//print ("else(items.Count == 0)");
		}		
		return v;
	}

	public void en(GameObject i)
	{
		items.Enqueue (i);
	}

	private IEnumerator ItemShooting() {

		while (true)
		{

			if (strat == true)
			{
				yield return new WaitForSeconds(0.1f);
				strat = false;              
			}
			else
			{
				if (!save_data.active_combo)
				{
					yield return new WaitForSeconds (1.5f);
				}
				else 
				{
					yield return new WaitForSeconds (3f);
				}
			}
		

			if (first_done) 
			{
				ItemShootingManager (0);
				first_done = false;
				second_done = true;
			} 

			else if (second_done) 
			{
				ItemShootingManager (1);
				second_done = false;

			} 

			else
			{
				ItemShootingManager (Random.Range (0, 7));
				//ItemShootingManager (10);
			}
		}
	}


	public void ItemShootingManager(int id)
	{
		switch (id)
		{

		case 0:
			print("case 0");
			save_data.active_combo = false;
			save_data.objectid = 0;
			GetItems().ConfigItem(Random.Range(0, 8)); 
			break;

		case 1:
			
			print("case 1");
			save_data.active_combo = false;
			save_data.objectid = 0;
			//GetItems().ConfigItem(Random.Range(0, 8));
			GetItems().ConfigItem(Random.Range(8, 22));
			break;

		case 2:
			
			print("case 2");
			save_data.active_combo = false;
			save_data.objectid = 0;
			GetItems().ConfigItem(Random.Range(0, 8));
			GetItems().ConfigItem(Random.Range(8, 22));
			break;

		case 3:

			print("case 2");
			save_data.active_combo = false;
			save_data.objectid = 0;
			GetItems().ConfigItem(Random.Range(8, 14));
			GetItems().ConfigItem(Random.Range(14, 22));
			break;
//		case 3:
//			print("case 3");
//			save_data.active_combo = false;
//			save_data.objectid = 0;
//			GetItems().ConfigItem(Random.Range(0, 4));
//			GetItems().ConfigItem(Random.Range(4, 8));
//			GetItems().ConfigItem(Random.Range(8, 14));
//			GetItems().ConfigItem(Random.Range(14, 21));
//
//			break;
//
//		case 4:
//			print("case 4");
//			save_data.active_combo = false;
//			save_data.objectid = 0;
//			GetItems().ConfigItem(Random.Range(0, 3));
//			GetItems().ConfigItem(Random.Range(3, 6));
//			GetItems().ConfigItem(Random.Range(6, 8));
//			GetItems().ConfigItem(Random.Range(8, 21));
//
//			break;
//
//		case 5:
//				print("case 5");
//			save_data.active_combo = false;
//			save_data.objectid = 0;
//			GetItems().ConfigItem(Random.Range(8, 12));
//			GetItems().ConfigItem(Random.Range(12, 16));
//			GetItems().ConfigItem(Random.Range(16, 21));
//			GetItems().ConfigItem(Random.Range(0, 8));
//
//			break;
//
//		case 6:
//				print("case 6");
//			save_data.active_combo = false;
//			save_data.objectid = 0;
//			GetItems().ConfigItem(Random.Range(8, 12));
//			GetItems().ConfigItem(Random.Range(12, 16));
//			GetItems().ConfigItem(Random.Range(16, 21));
//			GetItems().ConfigItem(Random.Range(0, 8));
//
//			break;
//
//		case 7:
//				print("case 7");
//			save_data.active_combo = false;
//			save_data.objectid = 0;
//			GetItems().ConfigItem(Random.Range(8, 12));
//			GetItems().ConfigItem(Random.Range(12, 16));
//			GetItems().ConfigItem(Random.Range(0, 8));
//			GetItems().ConfigItem(Random.Range(16, 23));
//
//			break;


			//-----combos------

		case 4:
			print("case 8");
			save_data.active_combo = true;
			save_data.groupid = Random.Range (0, 6);
			GetItems ().ConfigItem (Random.Range (0, 2));
			GetItems ().ConfigItem (Random.Range (2, 4));
			print ("combo true" + save_data.groupid.ToString ());

			break;

		case 5:
			print("case 9");
			save_data.active_combo = true;
			save_data.groupid =Random.Range (0, 6);
			GetItems ().ConfigItem (Random.Range (0, 2));
			GetItems().ConfigItem(Random.Range(2, 4));
			GetItems().ConfigItem(Random.Range(4, 6));

			print ("combo true" + save_data.groupid.ToString ());

			break;

		case 6:
			print("case 10");
			save_data.active_combo = true;
			save_data.groupid =Random.Range (0,6);
			GetItems ().ConfigItem (Random.Range (0, 2));
			GetItems().ConfigItem(Random.Range(2, 4));
			GetItems().ConfigItem(Random.Range(4, 6));
			GetItems().ConfigItem(Random.Range(6, 7));

			print ("combo true" + save_data.groupid.ToString ());
			break;

		}
	}


}
