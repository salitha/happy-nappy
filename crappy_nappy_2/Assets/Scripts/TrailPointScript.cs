﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TrailPointScript : MonoBehaviour {

    public GameObject objTrail;
	public GameObject objRedEffect;
	public GameObject objGldnRtlFX;
	private GameObject objtrail2,objtrail3,objtrail4;
	public ScoreEfx objScoreEfx;
	private ScoreEfx objScoreEfx2;
    public GameObject script_obj;
	public GameObject footer;
	public GameObject score_obj;
	public GameObject objGldnTm,gldnFX;

	public NewPool newpool;

	public bool bonus=false;
	//private bool bGldnTm = false;
	public static bool bGldnTm = false;

	public int timmer;
	public int startime;

	public int idValu;

    int count = 0;
    int c = 1;
    public GameManager GmManager;
    //public Text combotxt;
    int bCount = 0;
    int gCount = 0;
    bool bBool = false;


    string myLog = "";

	public AudioSource good_item;
	public AudioSource Bad_item;

    void Start()
	{
		newpool = NewPool.Instance;
	
	}
   
    void Update()
	{
		timmer = Mathf.FloorToInt (Time.time);

		if (bonus && timmer == startime + 10) 
		{
			bonus = false;
			footer.GetComponent<FooterScript> ().active = true;
		}
	}

	private TestItems m_CollidingTestItemRef;

	bool bBonus=false;
	void OnTriggerEnter(Collider other)
	{
		m_CollidingTestItemRef = other.gameObject.GetComponentInParent<TestItems> ();
//		bool goodBad = other.gameObject.GetComponentInParent<TestItems> ().goodItem;
//		bBonus = other.gameObject.GetComponentInParent<TestItems> ().bBonus;

		if (m_CollidingTestItemRef.MyType == 0) {

			objtrail3 = (GameObject)Instantiate (objRedEffect, other.transform.position, Quaternion.identity);
			objtrail3.SetActive (true);

			if (bGldnTm) {
				objScoreEfx2 = (ScoreEfx)Instantiate (objScoreEfx, other.transform.position, Quaternion.identity);
				objScoreEfx2.UpdtSprt (9); // +20
				objScoreEfx2.gameObject.SetActive (true);
			} else {
				objScoreEfx2 = (ScoreEfx)Instantiate (objScoreEfx, other.transform.position, Quaternion.identity);
				objScoreEfx2.UpdtSprt (1);
				objScoreEfx2.gameObject.SetActive (true);
			}

			bBool = false;
			GmManager.slicecount = 0;
			good_item.Play ();
            //Mark: - Salitha Add
            good_item.volume = PlayerPrefs.GetFloat("Sound Volume");
			//other.gameObject.SetActive (false);
			m_CollidingTestItemRef.Reset ();

			bonues_item (true);
			bonues_x (true);

		} else if (m_CollidingTestItemRef.MyType == 1) {

			bBonus = true;

			objtrail4 = (GameObject)Instantiate (objGldnRtlFX, other.transform.position, Quaternion.identity);
			objtrail4.SetActive (true);

			m_CollidingTestItemRef.Reset ();

			//add score +25
			objScoreEfx2 = (ScoreEfx)Instantiate (objScoreEfx, other.transform.position, Quaternion.identity);
			objScoreEfx2.UpdtSprt (8);
			objScoreEfx2.gameObject.SetActive (true);

			//activate golden time mode
			objGldnTm.SetActive(true);
			gldnFX.SetActive (true);

			bGldnTm = true;
			TestItems.bGLD = true;


			Invoke ("KillGoldnTm", 15);

			bonues_item (false);
			bonues_x (false);
		}
		else {
			
			objtrail2 = (GameObject)Instantiate (objTrail, other.transform.position, Quaternion.identity);
			objtrail2.SetActive (true);

			if (bGldnTm) {
				objScoreEfx2 = (ScoreEfx)Instantiate (objScoreEfx, other.transform.position, Quaternion.identity);

				if(Scare_Meter.hppyMdFactor>1)
					objScoreEfx2.UpdtSprt (16); // +50 x10
				else
					objScoreEfx2.UpdtSprt (10); // +10

				objScoreEfx2.gameObject.SetActive (true);
			} else {
				objScoreEfx2 = (ScoreEfx)Instantiate (objScoreEfx, other.transform.position, Quaternion.identity);

				if(Scare_Meter.hppyMdFactor>1)
					objScoreEfx2.UpdtSprt (SetHappyScoreFX());
				else
					objScoreEfx2.UpdtSprt (0);	// +5

				objScoreEfx2.gameObject.SetActive (true);

				bonues_item (false);
				bonues_x (false);
			}

			bBool = true;
			combo ();
			Bad_item.Play ();
            //Mark: - Salitha Add
            Bad_item.volume = PlayerPrefs.GetFloat("Sound Volume");
            m_CollidingTestItemRef.Reset ();
		}
	}


	int SetHappyScoreFX()
	{
		switch (Scare_Meter.hppyMdFactor) {
		case 2:
			return 10;	// 10
			break;
		case 3:
			return 11;	//15
			break;
		case 4:
			return 9;	//20
			break;
		case 5:
			return 8;	//25
			break;
		case 6:
			return 12;	//30
			break;
		case 7:
			return 13;	//35
			break;
		case 8:
			return 14;	//40
			break;
		case 9:
			return 15;	//45
			break;
		case 10:
			return 16;	//50
			break;
		}

		return 0;
	}


	public ParticleSystem PS;
	int gldnFXkillCnt=0;
	void KillGoldnTm(){

		if (gldnFXkillCnt == 5) {
			//deactivate golden time mode
			bBonus = false;
			bGldnTm = false;
			objGldnTm.SetActive (false);
			gldnFX.SetActive (false);
			gldnFXkillCnt = 0;

			Color c= objGldnTm.GetComponent<SpriteRenderer> ().color;
			c.a =1;
			objGldnTm.GetComponent<SpriteRenderer> ().color = c;

			var m = PS.main;
			c = m.startColor.color;
			c.a =1;
			m.startColor = c;

			TestItems.bGLD = false;


		} else {
			gldnFXkillCnt++;
			Color c= objGldnTm.GetComponent<SpriteRenderer> ().color;
			c.a -= 0.15f;
			objGldnTm.GetComponent<SpriteRenderer> ().color = c;

			var m = PS.main;
			c = m.startColor.color;
			c.a -= 0.15f;
			m.startColor = c;


			Invoke ("KillGoldnTm", 1);
		}

	}

    void combo()
    {
        myLog = "combo()";
        if (GmManager.slicecount == 0 && bBool == true)

        {
            //bCount = 1;
            GmManager.slicecount=1;
            myLog = "GmManager.slicecount=1;";
        }
        else if(GmManager.slicecount ==1 && bBool == true)
        {
           // bCount = 2;
            GmManager.slicecount = 2;
            myLog = "GmManager.slicecount = 2;";
        }
        else if(GmManager.slicecount == 2 && bBool == true)
        {
            GmManager.slicecount = 3;
            myLog = "mManager.slicecount = 3;";
        }
        else if(GmManager.slicecount == 3 && bBool == true)
        {
            GmManager.slicecount = 4;
            myLog = "GmManager.slicecount = 4;";
        }
        else if (GmManager.slicecount == 4 && bBool == true)
        {
            GmManager.slicecount = 5;
            myLog = "GmManager.slicecount = 5;";
        }
		else if (GmManager.slicecount == 5 && bBool == true)
		{
			GmManager.slicecount = 6;
			myLog = "GmManager.slicecount = 6;";
		}
        else 
        {
            GmManager.slicecount = 0;
            myLog = " GmManager.slicecount = 0";
        }
        //count++;

        //if (count == 2 && GmManager.combospattern == true)
        //{
        //    combotxt.text = "COMBO 2X" + c + " !!!";
        //}
        //if (count == 3 && GmManager.combospattern == true)
        //{
        //    combotxt.text = "COMBO 3X" + c + " !!!";
        //}
        //if (count == 4 && GmManager.combospattern == true)
        //{
        //    combotxt.text = "COMBO 4X" + c + " !!!";
        //}
        //if (count > 4 && GmManager.combospattern == true)
        //{
        //    combotxt.text = "COMBO 4X" + c + " !!!";
        //}
        //else
        //{
        //    combotxt.text = "COMBO 4X" + c + " !!!";
        //}
    }

	void bonues_x(bool goodBad)
	{
		if (bGldnTm) {
			script_obj.GetComponent<Score_Manager> ().slice_wrong = false;
		
		} else {
			if (goodBad) {
				script_obj.GetComponent<Score_Manager> ().slice_wrong = true;
			} else {
				script_obj.GetComponent<Score_Manager> ().slice_wrong = false;
			}
		}
	}

	void bonues_item (bool goodBad)
	{
		
		script_obj.GetComponent<Scare_Meter> ().calc_meter_trailpoint (goodBad,bGldnTm);

		if(bBonus)
			script_obj.GetComponent<Score_Manager> ().set_score (25,goodBad,bGldnTm,1);
		else
			script_obj.GetComponent<Score_Manager> ().set_score (0,goodBad,bGldnTm,Scare_Meter.hppyMdFactor);
	
	}

}
