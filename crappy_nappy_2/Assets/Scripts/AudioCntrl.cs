﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioCntrl : MonoBehaviour {

	public AudioSource normalTime, happyTime, goldenTime, badTime, T1, T2;
	public static int aCode=0;

    private float m_musicVolume;
    private float m_soundVolumeGet;
    private float m_musicVolumeGet;

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        volumeLevel();
		switch (aCode) {
		case 1:
			if (normalTime.volume > 0.9f) {
				normalTime.volume = m_musicVolume;
				happyTime.volume = 0;
				goldenTime.volume = 0;
				badTime.volume = 0;
			} else {
				normalTime.volume = Mathf.Lerp (normalTime.volume, m_musicVolume, 0.1f);
				happyTime.volume = Mathf.Lerp (happyTime.volume, 0, 0.1f);
				goldenTime.volume = Mathf.Lerp (goldenTime.volume, 0, 0.1f);
				badTime.volume = Mathf.Lerp (badTime.volume, 0, 0.1f);
			}

			break;
		case 2:
			if (happyTime.volume > 0.9f) {
				normalTime.volume = 0;
				happyTime.volume = m_musicVolume;
				goldenTime.volume = 0;
				badTime.volume = 0;
			} else {
				normalTime.volume = Mathf.Lerp (normalTime.volume, 0, 0.1f);
				happyTime.volume = Mathf.Lerp (happyTime.volume, m_musicVolume, 0.1f);
				goldenTime.volume = Mathf.Lerp (goldenTime.volume, 0, 0.1f);
				badTime.volume = Mathf.Lerp (badTime.volume, 0, 0.1f);
			}
			break;	
		case 3:
			if (goldenTime.volume > 0.9f) {
				normalTime.volume = 0;
				happyTime.volume = 0;
				goldenTime.volume = m_musicVolume;
				badTime.volume = 0;
			} else {
				normalTime.volume = Mathf.Lerp (normalTime.volume, 0, 0.1f);
				happyTime.volume = Mathf.Lerp (happyTime.volume, 0, 0.1f);
				goldenTime.volume = Mathf.Lerp (goldenTime.volume, m_musicVolume, 0.1f);
				badTime.volume = Mathf.Lerp (badTime.volume, 0, 0.1f);
			}
			break;	
		case 4:
			if (badTime.volume > 0.9f) {
				normalTime.volume = 0;
				happyTime.volume = 0;
				goldenTime.volume = 0;
				badTime.volume = m_musicVolume;
			} else {
				normalTime.volume = Mathf.Lerp (normalTime.volume, 0, 0.1f);
				happyTime.volume = Mathf.Lerp (happyTime.volume, 0, 0.1f);
				goldenTime.volume = Mathf.Lerp (goldenTime.volume, 0, 0.1f);
				badTime.volume = Mathf.Lerp (badTime.volume, m_musicVolume, 0.1f);
			}
			break;	
		}

	}

    //MARK: - Salitha Add
    void volumeLevel()
    { 
        m_musicVolumeGet = PlayerPrefs.GetFloat("Music Volume");

        if (m_musicVolumeGet > 0)
        {
            m_musicVolume = m_musicVolumeGet;
        }
        else
        {
            m_musicVolume = 0;
            normalTime.volume = 0;
            happyTime.volume = 0;
            goldenTime.volume = 0;
            badTime.volume = 0;
            T1.volume = 0;
            T2.volume = 0;
        }
    }
}
