﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestItems : MonoBehaviour {

	public static bool itemsLive=false;


	public bool b3rgGen=false;
	public string strPathName = "";
	public float f3rdgenSpd = 0;
//	public bool goodItem=true;
	public bool killready = false;
//	public bool bBonus=false;
	public bool bCombo=false;
	public bool goStraight=false;
	public Vector3 forceDir;

	public float force=3f;
	//public float grvt;
	public Rigidbody rig2d;

	private GameObject objItem;


	public GameObject GLDPFX;
	public static bool bGLD=false;

	private GameObject pGPFX;

	private Transform m_MyTransform;

	void Awake()
	{
		m_MyTransform = transform;

		objItem = m_MyTransform.GetChild (0).gameObject;
		m_MyCollider = objItem.GetComponent<Collider> ();
	}

	private Collider m_MyCollider;
	private TestMode m_TestModeRef;
	private int m_MyType, m_MyQueueIndex;
	public int MyType
	{
		get { return m_MyType; }
	}

	private bool m_DidGoToGameArea = false;
	public bool DidGoToGameArea
	{
		get { return m_DidGoToGameArea; }
		set { m_DidGoToGameArea = value; }
	}

	public void InitiateObject(int _choosenType, int _queIndex, TestMode _testModeRef)
	{
		m_MyType = _choosenType;
		m_MyQueueIndex = _queIndex;
		m_TestModeRef = _testModeRef;

		m_MyCollider.enabled = false;

		DidGoToGameArea = false;
	}

	private bool m_HasMyBehaviourStarted = false;
	public void StartMyBehaviours()
	{
		if (m_MyType != 0) { // Bonus Item or Bad Item

			m_MyTransform.rotation = Quaternion.Euler (new Vector3 (180, m_MyTransform.rotation.y, m_MyTransform.rotation.z));
		}

		if (bGLD) {
			if (m_TestModeRef.GLDPFXQueue.Count == 0) {
				pGPFX = Instantiate (GLDPFX, objItem.transform.position, objItem.transform.rotation);
			} else {
				pGPFX = m_TestModeRef.GLDPFXQueue.Dequeue () as GameObject;
			}
			pGPFX.transform.SetParent (objItem.transform);
			pGPFX.transform.localPosition = Vector3.zero;
			pGPFX.transform.localRotation = Quaternion.Euler (Vector3.zero);
			pGPFX.SetActive (true);
		}
			
		rig2d.isKinematic = false;

		if (!b3rgGen) {
			
			if (goStraight) {

				rig2d.useGravity = false;
				rig2d.velocity = forceDir * 10;
				AddMyTouque ();

			} else {
				rig2d.AddForce (forceDir * force, ForceMode.Impulse);
				AddMyTouque ();
			}
		}else { // For Items that goes in ITween path, we only add a torque
			AddMyTouque ();
		}

		ItemAudio();
		Invoke ("KillReady", 1);
		//Invoke ("KillMe", 4);

		m_HasMyBehaviourStarted = true;
		m_MyCollider.enabled = true;
	}

	void KillReady(){
		killready = true;
	}

	public AudioClip tossA1, tossA2, tossA3;
	void ItemAudio(){
        
        AudioSource aud = gameObject.GetComponent<AudioSource>();
        //MARK: - Salitha Add
        float m_soundVolume = PlayerPrefs.GetFloat("Sound Volume");
        aud.volume = m_soundVolume;
        //end
		int rndmA = Random.Range (0, 3);

		if (rndmA == 0)
			aud.clip = tossA1;
		else if (rndmA == 1)
			aud.clip = tossA2;
		else
			aud.clip = tossA3;

		aud.Play ();
	}

	public void GOGO(){
		Vector3[] tmpVs = iTweenPath.GetPath (strPathName);
		//tmpVs [0].z = -10;
		gameObject.transform.position = tmpVs [0];
		gameObject.transform.rotation = Quaternion.identity;
		iTween.MoveTo (gameObject, iTween.Hash ("path", iTweenPath.GetPath (strPathName), "time", f3rdgenSpd,"easetype",iTween.EaseType.linear));
		//AddMyTouque ();
	}

	void KillMe(){

		Reset ();
	}

	// Update is called once per frame
	void Update () {

		if (m_HasMyBehaviourStarted) {
			if (TestMode.bGameOver) {
				Reset ();
			} else
				itemsLive = true;
		}
	}



	void AddMyTouque(){
		Vector3 torq = Vector3.zero;

		torq.x = Random.Range (-100, 200);
		torq.y = Random.Range (-100, 200);
		torq.z = Random.Range (-100, 200);

		//if (goodItem) {
		if(MyType == 0) {// Good Item
			rig2d.AddTorque (torq);
		} else {
			Invoke ("AddMyTouque4bad", 0.75f);
		}

		rig2d.AddTorque (torq);
	}

	void AddMyTouque4bad(){

		Vector3 torq = Vector3.zero;

		/*
		torq.x = Random.Range (-55,55);
		torq.y = Random.Range (-55,55);
		torq.z = Random.Range (-55,55);*/

		torq.x = Random.Range (40,90);
		torq.y = Random.Range (40,90);
		torq.z = Random.Range (40,90);

		rig2d.AddTorque (torq);

	}

	void OnCollisionEnter(Collision col){

		if (col.gameObject.transform.parent != null) {

			if (col.gameObject.transform.parent.tag != "good" && killready == true) {
				Reset ();
			}
		}
	
	}

	public void Reset()
	{
		if(m_MyType != 1)
			m_TestModeRef.ResetItemCountInPattern++;

		DidGoToGameArea = false;

		m_MyCollider.enabled = false;
		m_HasMyBehaviourStarted = false;

		itemsLive = false;

		if (!System.String.IsNullOrEmpty (strPathName)) {
			iTween.Stop (gameObject);
		} 

		b3rgGen=false;
		strPathName = "";
		f3rdgenSpd = 0;
//		goodItem=true;
		killready = false;
//		bBonus=false;
		bCombo=false;
		goStraight=false;
		forceDir = Vector3.zero;

		rig2d.velocity = Vector3.zero;
		rig2d.isKinematic = true;
		rig2d.useGravity = true;

		m_MyTransform.position = new Vector3 (-1000f, -1000f, 0);

		if (m_MyType == 0) { // Good
			switch (m_MyQueueIndex) {
			case 0:
				m_TestModeRef.GoodObject_0.Enqueue (gameObject);
				break;
			case 1:
				m_TestModeRef.GoodObject_1.Enqueue (gameObject);
				break;
			case 2:
				m_TestModeRef.GoodObject_2.Enqueue (gameObject);
				break;
			case 3:
				m_TestModeRef.GoodObject_3.Enqueue (gameObject);
				break;
			case 4:
				m_TestModeRef.GoodObject_4.Enqueue (gameObject);
				break;
			case 5:
				m_TestModeRef.GoodObject_5.Enqueue (gameObject);
				break;
			case 6:
				m_TestModeRef.GoodObject_6.Enqueue (gameObject);
				break;
			case 7:
				m_TestModeRef.GoodObject_7.Enqueue (gameObject);
				break;
			}
		} else if (m_MyType == 1) { // Bonus
			m_TestModeRef.BonusObject.Enqueue(gameObject);
		} else { // Bad
			switch (m_MyQueueIndex) {
			case 0:
				m_TestModeRef.BadObject_0.Enqueue (gameObject);
				break;
			case 1:
				m_TestModeRef.BadObject_1.Enqueue (gameObject);
				break;
			case 2:
				m_TestModeRef.BadObject_2.Enqueue (gameObject);
				break;
			case 3:
				m_TestModeRef.BadObject_3.Enqueue (gameObject);
				break;
			case 4:
				m_TestModeRef.BadObject_4.Enqueue (gameObject);
				break;
			case 5:
				m_TestModeRef.BadObject_5.Enqueue (gameObject);
				break;
			case 6:
				m_TestModeRef.BadObject_6.Enqueue (gameObject);
				break;
			case 7:
				m_TestModeRef.BadObject_7.Enqueue (gameObject);
				break;
			}
		}

		if (MyType == 1) {
			gameObject.SetActive (false);
		}

		if (pGPFX) {
			pGPFX.transform.parent = null;
			pGPFX.transform.position = new Vector3 (-1000f, -1000f, 0);
			m_TestModeRef.GLDPFXQueue.Enqueue (pGPFX);
			pGPFX.SetActive (false);
			pGPFX = null;
		}
	}
}
