﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Scare_Meter : MonoBehaviour {

	private Score_Manager score_Mngr;

	public static Scare_Meter Instance{ set; get;}

	public GameObject xhpyTmFX1, xhpyTmFX2, xhpyTmFX3;
	public Animator hpyTmAnim1;

	public float max_Helth;
	public float cur_Helth;
	public float calc_Helth;

	public GameObject pausebtn;
	public GameObject helthBar;
//	public GameObject GameOver;
	private GameObject script_obj;

	public Sprite[] xFacterSprt = new Sprite[9];
	public SpriteRenderer spRndr;

	public GameObject baby;

	public GameObject objRedAlarm;
	private Color cRedAlmColor,cRedAlmColor2;
	private SpriteRenderer imgRedAlrm;


	 void Awake()
	{
		Instance = this;
	}

	// Use this for initialization
	void Start () {

		TestMode.bGameOver = false;

		max_Helth = 100f;
		cur_Helth = 0f;
		cur_Helth = max_Helth;

		score_Mngr = Score_Manager.Instance;

		imgRedAlrm = objRedAlarm.GetComponent<SpriteRenderer> ();
		cRedAlmColor = objRedAlarm.GetComponent<SpriteRenderer> ().color;
		cRedAlmColor2 = cRedAlmColor;

		Invoke ("CheckHappy", 1);
	}

	Color cFadeFX;
	int iFadeCnt=0;
	void FadeAnim(){
		iFadeCnt++;

		cFadeFX = xhpyTmFX2.GetComponent<SpriteRenderer>().color;
		cFadeFX.a = Mathf.Lerp (cFadeFX.a, 0, 0.2f);
		xhpyTmFX2.GetComponent<SpriteRenderer> ().color = cFadeFX;

		cFadeFX = xhpyTmFX3.GetComponent<SpriteRenderer> ().color;
		cFadeFX.a = Mathf.Lerp (cFadeFX.a, 0, 0.2f);
		xhpyTmFX3.GetComponent<SpriteRenderer> ().color = cFadeFX;

		if (iFadeCnt == 6) {
			xhpyTmFX2.SetActive (false);
			xhpyTmFX3.SetActive (false);
			iFadeCnt = 0;
			cFadeFX = xhpyTmFX2.GetComponent<SpriteRenderer>().color;
			cFadeFX.a = 1;
			xhpyTmFX2.GetComponent<SpriteRenderer> ().color = cFadeFX;
			cFadeFX = xhpyTmFX3.GetComponent<SpriteRenderer>().color;
			cFadeFX.a = 1;
			xhpyTmFX3.GetComponent<SpriteRenderer> ().color = cFadeFX;

		}
		else
			Invoke ("FadeAnim", 0.05f);
	}

	int hppyCnt=0;
	bool hppyMode=false;
	public static int hppyMdFactor=1;
	public GameObject hpyTMBG;
	bool bRed=false;
	float fRed=0,fRed2=0;

	void CheckHappy(){
		
		if (cur_Helth > 99) {
			hppyCnt++;
		} else {
			hppyCnt = 0;
			if (hppyMode) {
				xhpyTmFX1.SetActive (false);
				Invoke ("FadeAnim", 0.1f);
				Debug.Log ("happy mode over!");
				hppyMode = false;
				hpyTMBG.SetActive (false);
				hppyMdFactor = 1;
				hppyCnt = 0;
			}
		}

		if (cur_Helth < 50) {
			cRedAlmColor = new Color (1f, 0, 0, 0);
			bRed = true;
		} else if (cur_Helth < 60) {
			cRedAlmColor = new Color (0.7f,0.7f,0.7f, 0);
			bRed = true;
		}else if (cur_Helth < 70) {
			cRedAlmColor = new Color (0.4f,0.4f,0.4f, 0);
			bRed = true;
		}else {
			objRedAlarm.SetActive (false);

			fRed = 1;
			//imgRedAlrm.color = cRedAlmColor;
			bRed = false;
		}
			

		if (hppyCnt == 15) { //15
			hppyMode = true;
			hpyTMBG.SetActive (true);
			Debug.Log ("happy mode x2");
			hppyMdFactor = 2;

			spRndr.sprite = xFacterSprt [0];

			xhpyTmFX1.SetActive (true);
			//hpyTmAnim1.Play ("xhpyTmFX1",-1,0);

			Invoke ("HpyFXAnim", 0.25f);
		} 

		if (hppyCnt == 19) { //19
			Debug.Log ("happy mode x3");
			hppyMdFactor = 3;

			spRndr.sprite = xFacterSprt [1];

			xhpyTmFX1.SetActive (true);
			//hpyTmAnim1.Play ("xhpyTmFX1",-1,0);

			Invoke ("HpyFXAnim", 0.25f);
		}

		if (hppyCnt == 23) { // 23
			Debug.Log ("happy mode x4");
			hppyMdFactor = 4;

			spRndr.sprite = xFacterSprt [2];

			xhpyTmFX1.SetActive (true);
			//hpyTmAnim1.Play ("xhpyTmFX1");

			Invoke ("HpyFXAnim", 0.25f);
		}

		if (hppyCnt == 27) {
			Debug.Log ("happy mode x5");
			hppyMdFactor = 5;

			spRndr.sprite = xFacterSprt [3];

			xhpyTmFX1.SetActive (true);
			//hpyTmAnim1.Play ("xhpyTmFX1");

			Invoke ("HpyFXAnim", 0.25f);
		}

		if (hppyCnt == 31) {
			Debug.Log ("happy mode x6");
			hppyMdFactor = 6;

			spRndr.sprite = xFacterSprt [4];

			xhpyTmFX1.SetActive (true);
			//hpyTmAnim1.Play ("xhpyTmFX1");

			Invoke ("HpyFXAnim", 0.25f);
		}

		if (hppyCnt == 35) {
			Debug.Log ("happy mode x7");
			hppyMdFactor = 7;

			spRndr.sprite = xFacterSprt [5];

			xhpyTmFX1.SetActive (true);
			//hpyTmAnim1.Play ("xhpyTmFX1");

			Invoke ("HpyFXAnim", 0.25f);
		}

		if (hppyCnt == 39) {
			Debug.Log ("happy mode x8");
			hppyMdFactor = 8;

			spRndr.sprite = xFacterSprt [6];

			xhpyTmFX1.SetActive (true);
			//hpyTmAnim1.Play ("xhpyTmFX1");

			Invoke ("HpyFXAnim", 0.25f);
		}

		if (hppyCnt == 43) {
			Debug.Log ("happy mode x9");
			hppyMdFactor = 9;

			spRndr.sprite = xFacterSprt [7];

			xhpyTmFX1.SetActive (true);
			//hpyTmAnim1.Play ("xhpyTmFX1");

			Invoke ("HpyFXAnim", 0.25f);
		}

		if (hppyCnt == 47) {
			Debug.Log ("happy mode x10");
			hppyMdFactor = 10;

			spRndr.sprite = xFacterSprt [8];

			xhpyTmFX1.SetActive (true);
			//hpyTmAnim1.Play ("xhpyTmFX1");

			Invoke ("HpyFXAnim", 0.25f);
		}

		if (TestItems.bGLD == true) {
			AudioCntrl.aCode = 3;
		} else if (hppyMode == true) {
			AudioCntrl.aCode = 2;
		} else if (bRed == true) {
			AudioCntrl.aCode = 4;
		} else{
			AudioCntrl.aCode = 1;}


		Invoke ("CheckHappy", 1);
	}

	void HpyFXAnim(){
		//xhpyTmFX2.SetActive (true);
		xhpyTmFX3.SetActive (true);
	}

	// Update is called once per frame
	void Update()
	{
		baby_animation ();

		if (bRed) {
			fRed2 = Mathf.PingPong (Time.time*2, 1);
			cRedAlmColor.a = fRed2;
			imgRedAlrm.color = cRedAlmColor;

			objRedAlarm.SetActive (true);
		}

	}

	public void calc_meter_trailpoint(bool goodBad,bool bGldnTm)
	{
		if (bGldnTm) { // When at golden timer we decrease the health bar for any item sliced
//			DecresingHelthBar ();
			DecreaseHealthBarWhenBadItemSliced();

		} else {
			if (!goodBad) { //Bad Item Sliced
			
				DecreaseHealthBarWhenBadItemSliced();
//				IncresingHelthBar_trailpoint ();
				//DecresingHelthBar ();
			} else { 	// Good item sliced
				//IncresingHelthBar_trailpoint ();
//				DecresingHelthBar ();


				WhenGoodItemSliced ();
			}
		}

	}

	// When Bad Item hit on footer
	//public void calc_meter_footer(bool bgldTm)
	public void WhenBadItemHitOnFooter(bool bgldTm)
	{
		if (!bgldTm) 
		{
			//IncresingHelthBar_footer ();
			WhenBadItemWasMissed();
		}
	}

	// Called when good item sliced
	//public void IncresingHelthBar_trailpoint()
	private void WhenGoodItemSliced()
	{
		cur_Helth -= 20.0f;

		calc_Helth = (cur_Helth / max_Helth);

		if (cur_Helth <= 1) {
			SetHelthBar (0);
			pausebtn.SetActive (false);

			StartCoroutine (game_over ());
			save_data.game_over = true;
		} else {
			SetHelthBar (calc_Helth);
		}


//		if (calc_Helth >= 0 && calc_Helth<1)
//		{
//			SetHelthBar(calc_Helth);
//		}

	}

	public poop poop1;
	public GameObject PoopObject;
	void PoopAnim(){
		//poop1.PoopAnim ();
		PoopObject.SetActive(true);
        //MARK: - Salitha Add
        PoopObject.GetComponent<AudioSource>().enabled = true;
	}

	// Should be called when bad hit on footer
	//public void IncresingHelthBar_footer()
	private void WhenBadItemWasMissed()
	{
		cur_Helth -= 33f;
		//cur_Helth -= 16.5f;
	
		calc_Helth = (cur_Helth / max_Helth);

		if (cur_Helth <= 1) {
			SetHelthBar (0);
			pausebtn.SetActive (false);

			StartCoroutine (game_over ());
			save_data.game_over = true;
		} else {
			SetHelthBar (calc_Helth);
		}

//		if (calc_Helth > 0 && calc_Helth < 1) {
//			SetHelthBar (calc_Helth);
//		} 
	}

	// When Bad Item Sliced
	//public void DecresingHelthBar()
	private void DecreaseHealthBarWhenBadItemSliced()
	{
		//print("Calling DecresingHelthBar");
		cur_Helth += 10.0f;


		float calc_Helth = (cur_Helth / max_Helth);

		if (cur_Helth > 100) {
			cur_Helth = 100;
			SetHelthBar (1);
		} else if (calc_Helth > 0 && calc_Helth < 1) {
			SetHelthBar (calc_Helth);
		} else {
			cur_Helth = 100;
			SetHelthBar(1);
		}

	}

	private IEnumerator game_over()
	{
		TestMode.bGameOver=true;
		yield  return new WaitForSeconds (1f);
		PoopAnim();
		//yield  return new WaitForSeconds (1f);
		//Time.timeScale = 0;

		//GameOver.SetActive(true);
	}

	public void SetHelthBar(float myHelth)
	{
	//	print("Calling SetHelthBar");
		//helthBar.transform.localScale = new Vector3(myHelth, helthBar.transform.localScale.y, helthBar.transform.localScale.z);
		//Debug.Log(myHelth.ToString());
		helthBar.GetComponent<Image> ().fillAmount = (1-myHelth);
	}





	public void baby_animation()
	{

	

				if (cur_Helth > 84f)
				{
					baby.GetComponentInChildren<Baby> ().playanimation (2);
		
				} 
				else if (cur_Helth >= 67f && cur_Helth < 84f) 
				{
					baby.GetComponentInChildren<Baby> ().playanimation (1);
				}
				else if (cur_Helth >= 50f && cur_Helth <67f) 
				{
					baby.GetComponentInChildren<Baby> ().playanimation (0);
				}
				else if (cur_Helth >=40f && cur_Helth <50f) 
				{
					baby.GetComponentInChildren<Baby> ().playanimation (3);
				}
				else if (cur_Helth >=25f && cur_Helth <40f) 
				{
					baby.GetComponentInChildren<Baby> ().playanimation (4);
				}
				else if (cur_Helth >= 10f && cur_Helth <25f) 
				{
					baby.GetComponentInChildren<Baby> ().playanimation (5);
				}
				else if (cur_Helth >= 2f && cur_Helth <10f) 
				{
					baby.GetComponentInChildren<Baby> ().playanimation (6);
				}
				else if ( cur_Helth <2f) 
				{
					baby.GetComponentInChildren<Baby> ().playanimation (7);
				}

	}

}
