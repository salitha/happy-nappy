﻿using UnityEngine;

public struct Item
{
    public int itemID;
	public int score;
    public float velocity;
    public Vector3 startPosition;
    public string objectTag;



	public Item(int ItemID, string ObjectTag, float Velocity, Vector3 StartPosition,int Score)
    {
        this.itemID = ItemID;
        this.objectTag = ObjectTag;
        this.velocity = Velocity;
        this.startPosition = StartPosition;
		this.score = Score;
    }
};
