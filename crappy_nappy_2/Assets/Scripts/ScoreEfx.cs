﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreEfx : MonoBehaviour {
	public bool bCmbFX = false;
	public float fCmdScl=0;
	public SpriteRenderer spRndr;
	public Sprite[] sprts = new Sprite[8];

	private Sprite crntSprt;
	private Vector3 vCmbScl=Vector3.zero;

	public void UpdtSprt(int scoreCode){
		switch (scoreCode) {
		case 0:
			crntSprt = sprts [0];	//+5
			break;
		case 1:
			crntSprt = sprts [1];	//-10
			break;
		case 5:
			crntSprt = sprts [5];	//x2
			break;
		case 6:
			crntSprt = sprts [6];	//x3
			break;
		case 7:
			crntSprt = sprts [7];	//x4
			break;
		case 8:
			crntSprt = sprts [2];	//+25
			break;
		case 9:
			crntSprt = sprts [3];	//+20
			break;
		case 10:
			crntSprt = sprts [4];	//+10
			break;
		case 11:
			crntSprt = sprts [8];	//+15
			break;
		case 12:
			crntSprt = sprts [9];	//+30
			break;
		case 13:
			crntSprt = sprts [10];	//+35
			break;
		case 14:
			crntSprt = sprts [11];	//+40
			break;
		case 15:
			crntSprt = sprts [12];	//+45
			break;
		case 16:
			crntSprt = sprts [13];	//+50
			break;
		case 17:
			crntSprt = sprts [14];	//X5
			break;
		case 18:
			crntSprt = sprts [15];	//X6
			break;
	}

		gameObject.GetComponent<SpriteRenderer> ().sprite = crntSprt;

	}
	// Use this for initialization
	void Start () {


		
	}
	
	// Update is called once per frame
	void LateUpdate () {
		//spRndr.sprite = crntSprt;
		//gameObject.GetComponent<SpriteRenderer> ().sprite=sprts[1];
		if (bCmbFX) {
			vCmbScl.x = fCmdScl;vCmbScl.y = fCmdScl;vCmbScl.z = fCmdScl;
			gameObject.transform.localScale += vCmbScl;
		}
	}

	void killme()
	{
		Destroy(gameObject);
	}
}
