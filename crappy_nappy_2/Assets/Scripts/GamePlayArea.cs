﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayArea : MonoBehaviour {

	void OnTriggerExit(Collider other)
	{
		other.gameObject.GetComponentInParent<TestItems> ().DidGoToGameArea = true;
	}
}
