﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public GameObject comboImg;
	public ScoreEfx objScoreEfx;
	private ScoreEfx objScoreEfx2;

	private Vector3 m_Po;
	Vector2 start_pos;
	Vector2 end_pos;

	float total_velocity;

	public GameObject slice;
	public GameObject point_partical;
	public GameObject point_trail;

	bool m_click = true;
	bool m_move = false;

	public Material[] point_material;

    float fDot, fDot2, fDif;
    Vector3 vDir = Vector3.zero;
    Vector3 vDir2;
    bool sign1, sign2;
    private Vector3 vPos, vMou, vTmp;
    public GameObject objCube;
    public Text combotxt;
    public bool combospattern;
    public int slicecount;

    string myLog = "";

    //public ParticleSystem ps;

    // Use this for initialization
    void Start()
	{
        slicecount = 0;
        point_change ();
		point_partical.GetComponent<ParticleSystem> ().GetComponent<Renderer> ().sortingOrder = 4;
		point_trail.GetComponent<TrailRenderer> ().sortingOrder = 4;
        combospattern = false;
 
    }

    void OnGUI()
    {
        // myLog = GUI.TextArea(new Rect(10, 10, Screen.width/3 - 10, Screen.height/3 - 10), myLog);
    }

        // Update is called once per frame
	private Vector3 pointT=new Vector3(0,0,1.5f);
	public AudioSource audSrc;
	public AudioClip swipeClip;
	private bool bSwipe = false;
    void Update()
	{
		if (Input.touchCount > 0) 
		{
			m_Po = Camera.main.ScreenToWorldPoint (new Vector3 (Input.GetTouch (0).position.x, Input.GetTouch (0).position.y, -99.8f)); //1
			m_Po.z = -45;//-99.8f; //-1

			switch (Input.GetTouch (0).phase)
			{
			case TouchPhase.Began:

				point_partical.GetComponent<ParticleSystem> ().Play ();
				start_pos = new Vector2 (m_Po.x, m_Po.y);
				slice.transform.position = m_Po;

				vDir = Vector3.zero;
				vPos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				vPos.z = 0;
                   // objCube.transform.position = vPos;



                break;

	    		case TouchPhase.Moved:

                    combospattern = true;
                    end_pos = new Vector2 (m_Po.x, m_Po.y);
					total_velocity = Vector2.Distance (start_pos, end_pos) / Time.smoothDeltaTime;
				    start_pos = new Vector2 (m_Po.x, m_Po.y);

				    if (total_velocity > 1f) 
				    {
						slice.GetComponent<BoxCollider> ().enabled = true;
					    slice.transform.position = m_Po;
						if (!bSwipe) {
							audSrc.clip = swipeClip;
							audSrc.Play ();
							bSwipe = true;
						}
				    }

                    vTmp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    vTmp.z = 0;

                    if (Vector3.Distance(vTmp, vPos) > 2)
                    {                     
                        if (vDir == Vector3.zero)
                        {
                            vDir = (vTmp - vPos).normalized;
                            sign1 = vDir.x > 0;
                        }

                        vDir2 = (vTmp - vPos).normalized;
                        sign2 = vDir2.x > 0;

                        //if (sign1 == sign2)
                        //{
                        //    Debug.Log("COMBO X1 !!!");
                        //    combotxt.text = "COMBO X1 !!!";
                        //}

                        if (sign1 != sign2)
                        {
                            Debug.Log("no combo!");
                            //combotxt.text = "no combo!";
                            myLog = "no combo!";
                            slicecount = 0;
                            //combotxt.text = " ";
                            combospattern = false;

                        }

                        fDif = vTmp.y - vPos.y;

                        if (Mathf.Abs(fDif) > 2 )
                        {
                            Debug.Log("No COMBO!");
                           //  combotxt.text = "No COMBO!";
                            myLog = "no combo!";
                            slicecount = 0;
                            //combotxt.text = " ";
                            combospattern = false;
                        }

                        if (combospattern == true)
                        {
                            myLog = "Else slicecount = " + slicecount;
                            combotxt.text = "Else slicecount = " + slicecount;
                            StartCoroutine(Combos());

                        }

                        // dbg = vDir.ToString() + " , " + vDir2.ToString() + " neg " + sign1.ToString() + " posi " + sign2.ToString() +
                        //    " dif " + fDif.ToString();
                        //Instantiate(objCube, vTmp, Quaternion.identity);
                        vPos = vTmp;
                    }

					

                    break;

			case TouchPhase.Ended:

				bSwipe = false;
				slice.GetComponent<BoxCollider> ().enabled = false;

				    point_partical.GetComponent<ParticleSystem>().Stop ();

                    slicecount = 0;
                    //combotxt.text = " ";
                    combospattern = false;

                break;

			case TouchPhase.Canceled:
				bSwipe = false;
				break;

			    case TouchPhase.Stationary:

				slice.GetComponent<BoxCollider> ().enabled = false;

				break;
			}
		} 
		else
		{
			Vector3 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			pos.z = -45;//-99.8f;//-1;

			if (Input.GetMouseButtonDown(0) && m_click) 
			{
				start_pos = new Vector2 (pos.x, pos.y);

				slice.transform.position = pos;

				m_click = false;
				m_move = true;

				point_partical.GetComponent<ParticleSystem>().Play ();


			}
			else if (Input.GetMouseButton (0) && m_move) 
			{

				end_pos = new Vector2 (pos.x, pos.y);
				total_velocity = Vector2.Distance (start_pos, end_pos) / Time.smoothDeltaTime;
				start_pos = new Vector2 (pos.x, pos.y);



				if (total_velocity > 1f) 
				{
					slice.GetComponent<BoxCollider> ().enabled = true;
					slice.transform.position = pos;

					if (!bSwipe) {
						audSrc.clip = swipeClip;
						audSrc.Play ();
						bSwipe = true;
					}
				}
				//print ("m_move");

			} 
			else if (Input.GetMouseButtonUp (0))
			{
				slice.GetComponent<BoxCollider> ().enabled = false;
				m_click = true;
				m_move = false;

				//print ("m_end");
				point_partical.GetComponent<ParticleSystem>().Stop ();
				bSwipe = false;
			}
		}


	}

	Vector3 cmbFXpos=new Vector3(0,2.5f,0);

    public IEnumerator Combos()
	{
		//objScoreEfx2 =  (ScoreEfx) Instantiate(objScoreEfx,cmbFXpos, Quaternion.identity);

        if (slicecount == 1)
        {

            //combotxt.text = "COMBO 1X !!!";
            //myLog = "COMBO 1X !!!";
            //yield return new WaitForSeconds(2f);
            //combotxt.text = " ";
            //myLog = " ";
        }
		if (slicecount == 2 && save_data.active_combo)
        {
			//objScoreEfx2 =  (ScoreEfx) Instantiate(objScoreEfx,cmbFXpos, Quaternion.identity);
//			objScoreEfx2.UpdtSprt (5);
//			objScoreEfx2.fCmdScl = 2;
//			objScoreEfx2.bCmbFX = true;
//			objScoreEfx2.gameObject.SetActive (true);
			comboImg.gameObject.GetComponent<SpriteRenderer>().sprite=objScoreEfx.sprts[5];
           // combotxt.text = "COMBO X2 !!!";
           // myLog = "COMBO X2 !!!";
            yield return new WaitForSeconds(2f);
			comboImg.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
           // combotxt.text = " ";
            //myLog = " ";
		
        }
		else if (slicecount == 3 && save_data.active_combo)
        {
			//objScoreEfx2 =  (ScoreEfx) Instantiate(objScoreEfx,cmbFXpos, Quaternion.identity);
//			objScoreEfx2.UpdtSprt (6);
//			objScoreEfx2.fCmdScl = 2;
//			objScoreEfx2.bCmbFX = true;
//			objScoreEfx2.gameObject.SetActive (true);
			comboImg.gameObject.GetComponent<SpriteRenderer>().sprite=objScoreEfx.sprts[6];
           // combotxt.text = "COMBO X3 !!!";
           // myLog = "COMBO X3 !!!";
            yield return new WaitForSeconds(2f);
			comboImg.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
           // combotxt.text = " ";
            //myLog = " ";
        }
		else if (slicecount == 4 && save_data.active_combo)
        {
			//objScoreEfx2 =  (ScoreEfx) Instantiate(objScoreEfx,cmbFXpos, Quaternion.identity);
//			objScoreEfx2.UpdtSprt (7);
//			objScoreEfx2.fCmdScl = 2;
//			objScoreEfx2.bCmbFX = true;
//			objScoreEfx2.gameObject.SetActive (true);
			comboImg.gameObject.GetComponent<SpriteRenderer>().sprite=objScoreEfx.sprts[7];
            //combotxt.text = "COMBO X4 !!!";
            //myLog = "COMBO X4 !!!";
            yield return new WaitForSeconds(2f);
			comboImg.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
           // combotxt.text = " ";
            //myLog = " ";
        }
		else if (slicecount == 5 && save_data.active_combo)
		{
			//objScoreEfx2 =  (ScoreEfx) Instantiate(objScoreEfx,cmbFXpos, Quaternion.identity);
//			objScoreEfx2.UpdtSprt (17);
//			objScoreEfx2.fCmdScl = 2;
//			objScoreEfx2.bCmbFX = true;
//			objScoreEfx2.gameObject.SetActive (true);
			comboImg.gameObject.GetComponent<SpriteRenderer>().sprite=objScoreEfx.sprts[14];
			//combotxt.text = "COMBO X4 !!!";
			//myLog = "COMBO X4 !!!";
			yield return new WaitForSeconds(2f);
			comboImg.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
			// combotxt.text = " ";
			//myLog = " ";
		}
		else if (slicecount == 6 && save_data.active_combo)
		{
			//objScoreEfx2 =  (ScoreEfx) Instantiate(objScoreEfx,cmbFXpos, Quaternion.identity);
//			objScoreEfx2.UpdtSprt (18);
//			objScoreEfx2.fCmdScl = 2;
//			objScoreEfx2.bCmbFX = true;
//			objScoreEfx2.gameObject.SetActive (true);
			comboImg.gameObject.GetComponent<SpriteRenderer>().sprite=objScoreEfx.sprts[15];
			//combotxt.text = "COMBO X4 !!!";
			//myLog = "COMBO X4 !!!";
			yield return new WaitForSeconds(2f);
			comboImg.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
			// combotxt.text = " ";
			//myLog = " ";
		}
        else
        {
           // yield return new WaitForSeconds(3f);
            combotxt.text = " ";
            combospattern = false;
        }
		//yield return new WaitForSeconds(2f);
		//comboImg.SetActive (false);
    }

	public void point_change()
	{
		//point_trail.SetActive (false);
		//point_partical.SetActive (false);
		/*
		if (save_data.back_id == 0) {
			point_trail.SetActive (true);
			point_trail.GetComponent<TrailRenderer> ().enabled = true;
			point_trail.GetComponent<TrailRenderer> ().material = point_material [0];
		} else if (save_data.back_id == 2) {
			point_partical.SetActive (true);
			point_partical.GetComponent<ParticleSystem> ().GetComponent<Renderer> ().material = point_material [10];
		} else if (save_data.back_id == 3) {	
			point_trail.SetActive (true);
			point_trail.GetComponent<TrailRenderer> ().enabled = true;
			point_trail.GetComponent<TrailRenderer> ().material = point_material [3];
			

		} else if (save_data.back_id == 4) {
			point_trail.SetActive (true);
			point_trail.GetComponent<TrailRenderer> ().enabled = true;
			point_trail.GetComponent<TrailRenderer> ().material = point_material [1];
		} else if (save_data.back_id == 5) {
			point_trail.SetActive (true);
			point_trail.GetComponent<TrailRenderer> ().enabled = true;
			point_trail.GetComponent<TrailRenderer> ().material = point_material [2];
		} else if (save_data.back_id == 6) {
			point_partical.SetActive (true);
			point_partical.GetComponent<ParticleSystem> ().GetComponent<Renderer> ().material = point_material [8];
		} else if (save_data.back_id == 7) {

			point_trail.SetActive (true);
			point_trail.GetComponent<TrailRenderer> ().enabled = true;
			point_trail.GetComponent<TrailRenderer> ().material = point_material [4];

		} else if (save_data.back_id == 8) {
			point_partical.SetActive (true);
			point_partical.GetComponent<ParticleSystem> ().GetComponent<Renderer> ().material = point_material [5];

		} else if (save_data.back_id == 9) {

			point_partical.SetActive (true);
			point_partical.GetComponent<ParticleSystem> ().GetComponent<Renderer> ().material = point_material [9];



		} else if (save_data.back_id == 10) {
			point_partical.SetActive (true);
			point_partical.GetComponent<ParticleSystem> ().GetComponent<Renderer> ().material = point_material [6];


		}
		*/

		point_trail.GetComponent<TrailRenderer> ().material = point_material [Random.Range(0,5)];
		point_partical.GetComponent<ParticleSystem> ().GetComponent<Renderer> ().material = point_material [Random.Range(0,5)];
	}
}
