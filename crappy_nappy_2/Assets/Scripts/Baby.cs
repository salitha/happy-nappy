﻿using UnityEngine;
using System.Collections;

//[ExecuteInEditMode]
public class Baby : MonoBehaviour {

	public SpriteRenderer body;

	public Sprite lefthandsprite;
	public Sprite lefthandspriteUp;
	public Sprite righthandsprite;
	public Sprite righthandspriteUp;
	public Sprite leftPoophandsprite;
	public Sprite rightPoophandsprite;
	public SpriteRenderer lefthand;
	public SpriteRenderer righthand;
	public SpriteRenderer leftLeg;
	public SpriteRenderer rightLeg;

	public SpriteRenderer diper;
	public Sprite dipersprite;
	public Sprite diperpoopsprite;
	public Sprite diperpoopsprite2;
	public int diperCode = 0;

	public bool sadArm=false;

	/*
	 * 0-black boy, 1-black girl,2-chinese boy,3-chinese girl,4-def girl,5-def boy,6-white boy,7-white girl
	*/
	public int babyCode;
	public Sprite[] headsprite;
	public SpriteRenderer head;

	private string animName="anim-";
	Animator animator;
	public Animator faceanimator;

	public SpriteRenderer babyfacerender;
	private Sprite[] babyBrown,babyWhite,babyChinese,babyBlack;
	private Sprite[] crntSprite;

	private Vector3[] facePos = new Vector3[]{	new Vector3(-0.35f,-0.7f,0f),	//	0 - idle
												new Vector3(0.42f,-0.7f,0f),	//	1 - happy	
												new Vector3(-0.2f,-0.7f,0f),	//	2 - smile 
												new Vector3(0.24f,-0.35f,0f),	//	3 - sad
												new Vector3(0.19f,-0.21f,0f),	//	4 - angry
												new Vector3(0.19f,-0.18f,0f),	//	5 -	angry
												new Vector3(-0.07f,-0.33f,0f),	//	6 -	poop 1
												new Vector3(-0.29f,-0.29f,0f)	//	7 -	poop 2
	};

	public AudioClip[] babyemotions;
	public AudioSource audio;


	void Start () {
		animator = gameObject.GetComponent<Animator> ();

		babyBrown = Resources.LoadAll<Sprite> (@"Sprites/brownbaby");
		babyWhite= Resources.LoadAll<Sprite> (@"Sprites/whitebaby");
		babyChinese= Resources.LoadAll<Sprite> (@"Sprites/chinesebaby");
		babyBlack= Resources.LoadAll<Sprite> (@"Sprites/blackbaby");

		//SetBabyRace (3);


	}

	// 0 = brown  1=black	2=white	3=chinese

	public bool isPlaying=false;

	public void EndofClip(){
		//Debug.Log ("endofclip");
		isPlaying = false;
	}

	int[] audioindx = new int[]{1,4,0,7,4,5,6,10,11,16,18,19,20,9,10,13,9,10,20};

	public void playanimation(int animcode){
		int auCode;
		/*if (!audio.isPlaying) {
			audio.clip = babyemotions [Random.Range(0,9)];
			audio.Play ();
			//audio.loop = true;
		}*/
		
		switch (animcode) {
		case 0:		// idle
			animator.Play ("babyIdle_1");
			isPlaying = true;
			babyfacerender.transform.localPosition = facePos [animcode];
			babyfacerender.sprite = crntSprite [3];	//3
			diperCode = 0;
			sadArm = false;
			break;
		case 1:		// happy
			animator.Play ("baby_m_happy");
			isPlaying = true;
			babyfacerender.transform.localPosition = facePos [animcode];
			babyfacerender.sprite = crntSprite [0];	//0
			diperCode = 0;
			sadArm = false;

			if (!audio.isPlaying) {
				auCode = Random.Range (0, 200);
				if (auCode == 1) {
					audio.clip = babyemotions [audioindx [Random.Range (0, 2)]]; //1 4
					audio.Play ();
				}
			}
			break;
		case 2:		// smile

		//	print ("smile");
			animator.Play ("baby_m_smile");
			isPlaying = true;
			babyfacerender.transform.localPosition = facePos [animcode];
			babyfacerender.sprite = crntSprite [2];	//2
			diperCode = 0;
			sadArm = false;

			if (!audio.isPlaying) {
				auCode = Random.Range (0, 200);
				if (auCode == 1) {
					audio.clip = babyemotions [audioindx [Random.Range (2, 4)]]; //0 7
					audio.Play ();
				}
			}
			break;
		case 3:		// sad
			animator.Play ("baby_m_sad");
			isPlaying = true;
			babyfacerender.transform.localPosition = facePos [animcode];
			babyfacerender.sprite = crntSprite [4];	//4
			diperCode = 0;
			sadArm = true;

			if (!audio.isPlaying) {
				auCode = Random.Range (0,200);
				if (auCode == 1) {
					audio.clip = babyemotions [audioindx [Random.Range (4, 7)]]; //0 7
					audio.Play ();
				}
			}

			break;
		case 4:		// angry 1
			animator.Play ("baby_m_angry1");
			isPlaying = true;
			babyfacerender.transform.localPosition = facePos [animcode];
			babyfacerender.sprite = crntSprite [5];	//5
			diperCode = 0;
			//sadArm = true;
			animName="angry1";

			if (!audio.isPlaying) {
				auCode = Random.Range (0, 200);
				if (auCode == 1) {
					audio.clip = babyemotions [audioindx [Random.Range (7, 10)]]; //0 7
					audio.Play ();
				}
			}

			break;
		case 5:		// angry 2
			animator.Play ("baby_m_angry2");
			isPlaying = true;
			babyfacerender.transform.localPosition = facePos [animcode];
			babyfacerender.sprite = crntSprite [5];	//5
			diperCode = 0;
			sadArm = true;

			if (!audio.isPlaying) {
				auCode = Random.Range (0, 200);
				if (auCode == 1) {
					audio.clip = babyemotions [audioindx [Random.Range (10, 13)]]; //0 7
					audio.Play ();
				}
			}

			break;
		case 6:		// poop 1
			animator.Play ("baby_m_poop1");
			isPlaying = true;
			babyfacerender.transform.localPosition = facePos [animcode];
			babyfacerender.sprite = crntSprite [6];	//6
			diperCode = 1;
			sadArm = true;

			if (!audio.isPlaying) {
				auCode = Random.Range (0, 60);
				if (auCode == 5) {
					audio.clip = babyemotions [audioindx [Random.Range (13, 16)]]; //0 7
					audio.Play ();
				}
			}

			break;
		case 7:		// poop 2
			animator.Play ("baby_m_poop2");
			isPlaying = true;
			babyfacerender.transform.localPosition = facePos [animcode];
			babyfacerender.sprite = crntSprite [7];	//7
			diperCode = 2;
			sadArm = true;

			if (!audio.isPlaying) {
				auCode = Random.Range (0, 100);
				if (auCode == 5) {
					audio.clip = babyemotions [audioindx [Random.Range (16, 19)]]; //0 7
					audio.Play ();
				}
			}

			break;
		}
	}

	private Color m_BlakColor = new Color (167f/ 255f , 117f/ 255f, 92f/ 255f,1);	//black --
	private Color m_BrownColor = new Color (246f/255f ,244f / 255f, 242f / 255f,1); // brown --
	private Color m_ChineeseColor = new Color (255f / 255f, 255f / 255f, 180f / 255f,1); // chineese -
	private Color m_WhiteColor = new Color (1, 1, 1,1); // white
	private Color m_SouthAmericanColor = new Color (244f / 255f,208f / 255f, 183f / 255f,1); // South American --

	void updateanimation()
	{
		if (sadArm) {
			lefthand.sprite = lefthandspriteUp;
			righthand.sprite = righthandspriteUp;
		} else {
			lefthand.sprite = lefthandsprite;
			righthand.sprite = righthandsprite;
		}

		switch (diperCode) {
		case 0:
			diper.sprite = dipersprite;
			break;
		case 1:
			diper.sprite = diperpoopsprite;
			break;
		case 2:
			diper.sprite = diperpoopsprite2;
			lefthand.sprite = leftPoophandsprite;
			righthand.sprite = rightPoophandsprite;
			break;
		}

		////
		SetBabyHeadAndColors ();
	}

	private void SetBabyHeadAndColors()
	{
		head.sprite = headsprite [babyCode];


		switch (babyCode) {
		case 0:
			body.color= m_BlakColor;	//black
			lefthand.color= m_BlakColor;
			righthand.color= m_BlakColor;
			leftLeg.color= m_BlakColor;
			rightLeg.color= m_BlakColor;
			crntSprite=babyBlack;
			break;
		case 1:
			body.color = m_BlakColor;	//black
			lefthand.color = m_BlakColor;
			righthand.color = m_BlakColor;
			leftLeg.color = m_BlakColor;
			rightLeg.color = m_BlakColor;
			crntSprite=babyBlack;
			break;
		case 2:
			body.color = m_ChineeseColor; // chinese
			lefthand.color= m_ChineeseColor;
			righthand.color= m_ChineeseColor;
			leftLeg.color= m_ChineeseColor;
			rightLeg.color= m_ChineeseColor;
			crntSprite=babyChinese;
			break;
		case 3:
			body.color = m_ChineeseColor; // chinese
			lefthand.color= m_ChineeseColor;
			righthand.color= m_ChineeseColor;
			leftLeg.color= m_ChineeseColor;
			rightLeg.color= m_ChineeseColor;
			crntSprite=babyChinese;
			break;
		case 4:
			body.color = m_BrownColor;	// def baby
			lefthand.color= m_BrownColor;
			righthand.color= m_BrownColor;
			leftLeg.color= m_BrownColor;
			rightLeg.color= m_BrownColor;
			crntSprite=babyBrown;
			break;
		case 5:
			body.color = m_BrownColor;	// def baby
			lefthand.color= m_BrownColor;
			righthand.color= m_BrownColor;
			leftLeg.color= m_BrownColor;
			rightLeg.color= m_BrownColor;
			crntSprite=babyBrown;
			break;
		case 6:
			body.color = m_WhiteColor;	// white 
			lefthand.color= m_WhiteColor;
			righthand.color= m_WhiteColor;
			leftLeg.color= m_WhiteColor;
			rightLeg.color= m_WhiteColor;
			crntSprite=babyWhite;
			break;
		case 7:
			body.color = m_WhiteColor;	// white 
			lefthand.color = m_WhiteColor;
			righthand.color = m_WhiteColor;
			leftLeg.color = m_WhiteColor;
			rightLeg.color = m_WhiteColor;
			crntSprite=babyWhite;
			break;

		case 8:
			body.color = m_SouthAmericanColor;	// Leo
			lefthand.color= m_SouthAmericanColor;
			righthand.color= m_SouthAmericanColor;
			leftLeg.color= m_SouthAmericanColor;
			rightLeg.color= m_SouthAmericanColor;
			crntSprite=babyBrown;
			break;

		case 9:
			body.color = m_SouthAmericanColor;	// Gabrialla
			lefthand.color= m_SouthAmericanColor;
			righthand.color= m_SouthAmericanColor;
			leftLeg.color= m_SouthAmericanColor;
			rightLeg.color= m_SouthAmericanColor;
			crntSprite=babyBrown;
			break;
		} 
	}

//
//	public int plyCode=0;
//	void OnGUI()
//	{
//		if (GUI.Button (new Rect (20, 20, 50, 50), animName)) {
//			plyCode++;
//			playanimation (plyCode);
//		}
//	}

	void Update () {

		if (Time.timeScale == 0)
			audio.Stop ();
		
		updateanimation ();
	}

}
