﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//MARK: - Salitha Change This Class
public class Audio_Manager : MonoBehaviour {

    public AudioSource SoundSrc;
	public Toggle Music_btn;
    public Toggle Sound_btn;

    private float m_soundVolume;
    private float m_musicVolume;
    
	// Use this for initialization
	void Start () 
	{
        m_soundVolume = PlayerPrefs.GetFloat("Sound Volume");
        m_musicVolume = PlayerPrefs.GetFloat("Music Volume");
        SoundSrc.volume = m_soundVolume;

        if (m_musicVolume > 0)
        {
            Music_btn.isOn = false;
        }
        else
        {
            Music_btn.isOn = true;
        }

        if(m_soundVolume > 0)
        {
            Sound_btn.isOn = false;
        }
        else
        {
            Sound_btn.isOn = true;
        }
    }
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void music_btn_on_off()
	{
        if(Music_btn.isOn)
        {
            PlayerPrefs.SetFloat("Music Volume",0);
        }
        else
        {
            PlayerPrefs.SetFloat("Music Volume", 1);
        }
	}

    public void sound_btn_on_off()
    {
        if (Sound_btn.isOn)
        {
            PlayerPrefs.SetFloat("Sound Volume", 0);
            SoundSrc.volume = 0;
        }
        else
        {
            PlayerPrefs.SetFloat("Sound Volume", 1);
            SoundSrc.volume = 1;
        }
    }

}
