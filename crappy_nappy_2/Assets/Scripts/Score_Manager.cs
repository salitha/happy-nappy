﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Facebook.Unity;
using Facebook.MiniJSON;

public class Score_Manager : MonoBehaviour {

	public static Score_Manager Instance { set; get; }
    FBHolder fbholder;
    
    NewPool newpool;
	Item item;

	public Text scoreText;
	public Text highscoreText;

	public ItemInfo item_info;

	public GameObject panel_red;
	public GameObject game_over;
	public GameObject newrecrd_img;

	public Animator highscore_ani;

	public int count = 0;
	public int timmer;
	public int bouns=10;
	public int count_x=1;
	public int lost_time=0;
	//public int highscore;
	public int score;
	public int new_highscore;

	public bool slice_wrong = false;
	public bool combo_score;

	void Awake()
	{
		Instance = this;
	}

	// Use this for initialization
	void Start () 
	{
		//PlayerPrefs.DeleteAll ();

		lost_time = Mathf.FloorToInt (Time.time);
		//PlayerPrefs.DeleteAll ();
		count_x=1;
		score = 0;
		scoreText.text = score.ToString ();

		int m_HighScore = PlayerPrefs.GetInt ("score"); 
		//highscore = PlayerPrefs.GetInt ("score");
		print (m_HighScore.ToString ());
		highscoreText.text = "BEST : " + m_HighScore.ToString ();

		int.TryParse (highscoreText.text.ToString (),out new_highscore);


		newpool = NewPool.Instance;
	}

	// Update is called once per frame
	void Update () 
	{

		//highscore = PlayerPrefs.GetInt ("score");


		 

		timmer = Mathf.FloorToInt (Time.time) - lost_time;



		if (timmer >= bouns) 
		{
			bouns += 10;
			count_x += 1;
		}
		if (slice_wrong   || save_data.game_over)
		{
			lost_time = lost_time + timmer;
			bouns = 10;
			count_x = 1;
			timmer = Mathf.FloorToInt (Time.time) - lost_time;
		}

	}

	public void set_score(int S,bool goodBad,bool bGldnTm,int hppyFactor)
	{
	//	int.TryParse (scoreText.text.ToString (), out score);

		if (S > 0) {
			score = score + S;
			S = 0;
		} else {
			
			if (bGldnTm) {
				hppyFactor = 10;
				if (goodBad)
					score = score + (20 * hppyFactor);
				else
					score = score + (10 * hppyFactor);

			} else {
				
				if (goodBad) {
					if (score > 10)
						score = score - 10;//+ (item.score * count_x);
					else
						score = 0;
				} else {
					score = score + (5 * hppyFactor);

				}
			}
		}

		scoreText.text = score.ToString ();	
		if (score > 0) 
		{
			
			scoreText.text = score.ToString ();	
		} 
		else 
		{
			scoreText.text= " 0";
		}

		combo_score = true;

		HighScore ();
	}

	public void active_combo(int id)
	{
		
//		int.TryParse (scoreText.text.ToString (), out score);
		item = item_info.ItemList [score];

		count++;


		save_data.active_combo = false;
		count = 0;

//		if (id < 8) {
//			score = score + (idd * newpool.count_x);
//			scoreText.text = score.ToString ();	
//			save_data.active_combo = false;
//			count = 0;
//			//	Debug.Log ("time_multiply");
//		} 
	}

	void AnimateNewrecord(){
		newrecrd_img.SetActive (true);
	}

	private bool scoreGate = false;
	public void HighScore()
	{
		//print ("call high score");

		if (PlayerPrefs.GetInt ("score") /*highscore*/== 0) 
		{
			Invoke ("AnimateNewrecord", 3);
		}
		if (score > PlayerPrefs.GetInt ("score") && score > 0 && scoreGate==false)
		{
			print ("go higher score");
			//highscore = score;
			Invoke ("AnimateNewrecord", 3);
			scoreGate = true;
			PlayerPrefs.SetInt ("score", /*highscore*/score);
		}

		if (score > /*highscore*/PlayerPrefs.GetInt ("score")) 
		{
            //var user = (Dictionary<string, object>)entry["user"];

            //highscore = score;
			print ("come high score"+score);
			PlayerPrefs.SetInt ("score", /*highscore*/score);

            //if (FB.IsLoggedIn)
            //{
            //    //fbholder.SetScore();
            //    string playerIDTemp = PlayerPrefs.GetString("playerID_Temp");
            //    PlayerPrefs.SetString("playerID", playerIDTemp);
            //}
            //else
            //{
            //    PlayerPrefs.SetString("playerID", "noID");
            //}
        }
	}

	public  void tempo()
	{
		if(new_highscore<score)
		{
			//panel_red.SetActive (true);
			highscore_ani.enabled = true;
		}
	}
		
}
