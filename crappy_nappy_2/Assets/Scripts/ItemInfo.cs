﻿using UnityEngine;
using System.Collections;

public class ItemInfo : MonoBehaviour {

    private static ItemInfo mInstance;
    public static ItemInfo Instance { get { return mInstance; } }

    public Item[] ItemList;

    void Awake()
    {
        mInstance = this;

        ItemList = new Item[23];

		ItemList[0] = new Item(0, "bad",  5.5f, new Vector3(-1, -8, 0),5);//Scarecrow (id,speed,velocity,GRAVITY)
		ItemList[1] = new Item(1, "bad",  5.5f, new Vector3(Random.Range(1, -1), -7, 0),8);//head
		ItemList[2] = new Item(2, "bad",  2.5f, new Vector3(6, Random.Range(3, 0), 0),10);//bat
		ItemList[3] = new Item(3, "bad",  7.5f, new Vector3(Random.Range(-2, 2), -8, 0),5);//RIP
		ItemList[4] = new Item(4, "bad",  6.5f, new Vector3(Random.Range(-2, 1), -8, 0),8);//joker
		ItemList[5] = new Item(5, "bad",  7.5f, new Vector3(Random.Range(-2, 2), -8, 0),5);//halloween ok
		ItemList[6] = new Item(6, "bad",  2.5f, new Vector3(-6, Random.Range(2.8f, 1), 0),8);//Casper
		ItemList[7] = new Item(7, "bad", -0.01f,new Vector3(Random.Range(-2, 2.5f), 7, 0),10);//Spider 

		ItemList[8] = new Item(8, "good", 6f, new Vector3(Random.Range(0, 2.5f), -8, 0),-5);//lettercubes
		ItemList[9] = new Item(9, "good", 6.5f, new Vector3(2, -8, 0),-8);//good2
		ItemList[10] = new Item(10, "good", 6.5f, new Vector3(Random.Range(-3, -0.5f), -8, 0),-10);//good3
		ItemList[11] = new Item(11, "good", 7.5f, new Vector3(Random.Range(-2, 0), -8, 0),-8);//ball
		ItemList[12] = new Item(12, "good", 7f, new Vector3(Random.Range(-1, 2), -8, 0),-5);//stroller
		ItemList[13] = new Item(13, "good", 6.5f, new Vector3(Random.Range(2, 0), -8, 0),-10);//socks
		ItemList[14] = new Item(14, "good", 6.5f,  new Vector3(Random.Range(-2, 2), -8, 0),-8);//bib
		ItemList[15] = new Item(15, "good", 6.5f, new Vector3(Random.Range(-2, 1), -8, 0),-5);//cloth

		ItemList[16] = new Item(16, "good", 7.5f, new Vector3(Random.Range(-1, 2.5f), -8, 0),-10);//teddybear
		ItemList[17] = new Item(17, "good", 6.5f, new Vector3(Random.Range(0, 2f), -8, 0),-8);//bath
		ItemList[18] = new Item(18, "good", 6.5f, new Vector3(Random.Range(0, -1), -8, 0),-5);//milk_bottle
		ItemList[19] = new Item(19, "good", 6.5f, new Vector3(Random.Range(-2, 1.5f), -8, 0),-8);//bawl
		ItemList[20] = new Item(20, "good", 6.5f,  new Vector3(Random.Range(-1, 2), -8, 0),-10);//duck

		ItemList[21] = new Item(21, "bad",  2.5f, new Vector3(6, Random.Range(3, 0), 0),10);//bonus1
		ItemList[22] = new Item(22, "bad",  2.5f, new Vector3(6, Random.Range(3, 0), 0),10);//bonus2


           
    }
}
