﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpPages : MonoBehaviour {

	public Image imgHelp;
	public Sprite img1, img2, img3, img4, img5;

	public int helpNmbr;
	public static int[] rndmCode=new int[2];

	// Use this for initialization
	void Start () {

		SetImage ();
	}

	void SetImage(){
		
		if (helpNmbr == 0) {
			rndmCode [0] = Random.Range (0, 5);
		} else {
			rndmCode [1] = Random.Range (0, 5);

			if (rndmCode [1] == rndmCode [0]) {
				SetImage ();
				return;
			}
		}

		switch (rndmCode[helpNmbr]) {
		case 0:
			imgHelp.sprite = img1;
			break;
		case 1:
			imgHelp.sprite = img2;
			break;
		case 2:
			imgHelp.sprite = img3;
			break;
		case 3:
			imgHelp.sprite = img4;
			break;
		case 4:
			imgHelp.sprite = img5;
			break;

		}
	}


	// Update is called once per frame
	void Update () {
		
	}
}
