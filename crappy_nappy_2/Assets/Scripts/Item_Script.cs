﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Item_Script : MonoBehaviour
{
	public ItemInfo itemInfo;

	public bool IsActive { set; get; }
	private bool isSliced = false;

	public static Item_Script Instance { set; get; }

	public float speed;
	public float force=3f;

	public int idd;

	public List<Sprite> sprites;

	//public SpriteRenderer Srenderer;

	private Rigidbody rig2d = null;

	public GameObject Effect;

	private Item item;

	private NewPool n;

	public Text txt;


	public int objectid=0;
	public Vector3 [,] combos_pos;

	Score_Manager sc_maneger;

	void Awake()
	{
		Instance = this;

		//combos_pos = n.combos_pos;

		//-------staright line--------
		save_data.combo [0,0] = new Vector3 (-4, -8, 1);
		save_data.combo  [0,1] = new Vector3 (0, -8, 1);
		save_data.combo  [0,2] = new Vector3 (4, -8, 1);
		save_data.combo  [0,3] = new Vector3 (8, -8, 1);
		save_data.combo  [1,0] = new Vector3 (-4, -8, 1);
		save_data.combo  [1,1] = new Vector3 (-1, -8, 1);
		save_data.combo  [1,2] = new Vector3 (2, -8, 1);
		save_data.combo  [1,3] = new Vector3 (5, -8, 1);

		//------cross line-----------
		save_data.combo  [2,0] = new Vector3 (4, -7, 1);
		save_data.combo  [2,1] = new Vector3 (1, -8, 1);
		save_data.combo  [2,2] = new Vector3 (-2, -6, 1);
		save_data.combo  [2,3] = new Vector3 (-5, -5, 1);

		save_data.combo  [3,0] = new Vector3 (-2, -7, 1);
		save_data.combo  [3,1] = new Vector3 (2, -8, 1);
		save_data.combo  [3,2] = new Vector3 (6, -6, 1);
		save_data.combo  [3,3] = new Vector3 (8, -5, 1);

		save_data.combo  [4,0] = new Vector3 (-3, -7, 1);
		save_data.combo  [4,1] = new Vector3 (1, -8, 1);
		save_data.combo  [4,2] = new Vector3 (4, -6, 1);
		save_data.combo  [4,3] = new Vector3 (7, -5, 1);

		save_data.combo  [5,0] = new Vector3 (5, -7, 1);
		save_data.combo  [5,1] = new Vector3 (1, -8, 1);
		save_data.combo  [5,2] = new Vector3 (-4, -6, 1);
		save_data.combo  [5,3] = new Vector3 (-7, -5, 1);


		rig2d = GetComponent<Rigidbody>();
	}
	void Start()
	{

		n = NewPool.Instance;
		sc_maneger = Score_Manager.Instance;
		combos_pos = n.combos_pos;


	}
		

	void Update()
	{
		
		if (save_data.game_over) 
		{
			this.gameObject.SetActive (false);

		}
		if (sc_maneger.score >= 150)
		{
			force = 3.2f;
		}




	}

	void AddMyTouque(){
		Vector3 torq=Vector3.zero;

		torq.x = Random.Range (-100, 200);
		torq.y = Random.Range (-100, 200);
		torq.z = Random.Range (-100,200);

		rig2d.AddTorque (torq);
	}

	public void ConfigItem(int id)
	{

		//print ("Item iD = "+id);
		//print ("combos_pos [save_data.groupid, save_data.objectid]" + combos_pos [save_data.groupid, save_data.objectid]);
		//reset();
		if(!rig2d) rig2d = GetComponent<Rigidbody>();
		rig2d.isKinematic = false;
		item = itemInfo.ItemList[id];

		idd = id;
		//Srenderer.sprite = sprites[id];
		int RandomVal = Random.Range(0, 100);
		int randomID = Random.Range(0, 23);


		if (id == 0) {//scarecrow
			print ("Item iD 00000 = "+id);

			if (!save_data.active_combo)
			{	
				if (RandomVal < 50) 
				{
					transform.position = new Vector3 (Random.Range (1, 5), -8, save_data.z_axixs [save_data.z_id]);
				} 
				else 
				{
					transform.position = new Vector3 (Random.Range (-5, -1), -8, save_data.z_axixs [save_data.z_id]);
				}
					
				rig2d.AddForce (new Vector2 (0, 5) * force, ForceMode.Impulse);

				AddMyTouque ();
				save_data.z_id++;
			} 
			else 
			{
				//print (save_data.groupid+" " +save_data.objectid);
				transform.position = save_data.combo [save_data.groupid, save_data.objectid];
				rig2d.AddForce (new Vector2 (0, 5) *force, ForceMode.Impulse);
				AddMyTouque();
				save_data.objectid++;


			}
		}
		else if (id == 1) 
		{//head
			if (!save_data.active_combo)
			{
				if (RandomVal < 50)
				{
					transform.position = new Vector3 (2, -7, save_data.z_axixs[save_data.z_id]);
					rig2d.AddForce (new Vector2 (-1, 6) * 2.5f, ForceMode.Impulse);
				
				} 
				else
				{
					transform.position = new Vector3 (-2, -7, save_data.z_axixs[save_data.z_id]);
					rig2d.AddForce (new Vector2 (1, 6) * 2.5f, ForceMode.Impulse);

				}

				AddMyTouque();
				save_data.z_id++;


			} 
			else 
			{
				//print (save_data.groupid+" " +save_data.objectid);
				transform.position = save_data.combo [save_data.groupid, save_data.objectid];
				rig2d.AddForce (new Vector2 (0, 5) * force, ForceMode.Impulse);
				AddMyTouque();
				save_data.objectid++;


			}
		} 
		else if (id == 2)
		{//bat
			if (!save_data.active_combo)
			{
				if (RandomVal < 50) 
				{
					transform.position = new Vector3 (7.5f, -3, save_data.z_axixs[save_data.z_id]);
					rig2d.AddForce (new Vector2 (-2, 4) * force, ForceMode.Impulse);

				} 
				else
				{
					transform.position = new Vector3 (-7.5f, -3,save_data.z_axixs[save_data.z_id]);
					rig2d.AddForce (new Vector2 (2, 4) * force, ForceMode.Impulse);

				}

				AddMyTouque();
				save_data.z_id++;
			} 
			else 
			{
				//print (save_data.groupid+" " +save_data.objectid);
				transform.position = save_data.combo [save_data.groupid, save_data.objectid];
				rig2d.AddForce (new Vector2 (0, 5) * force, ForceMode.Impulse);
				AddMyTouque();
				save_data.objectid++;

			}
		}
		else if (id == 3)
		{//RIP

			if (!save_data.active_combo)
			{
				if (RandomVal < 50) 
				{
					transform.position = new Vector3 (-1, -8, save_data.z_axixs[save_data.z_id]);
					rig2d.AddForce (new Vector2 (1, 4) * force, ForceMode.Impulse);

				} 
				else
				{
					transform.position = new Vector3 (1, -8, save_data.z_axixs[save_data.z_id]);
					rig2d.AddForce (new Vector2 (-1, 4) * force, ForceMode.Impulse);


				}
				AddMyTouque();
				save_data.z_id++;
			}
			else 
			{	
				//print (save_data.groupid+" "+save_data.objectid);
				transform.position = save_data.combo [save_data.groupid, save_data.objectid];
				rig2d.AddForce (new Vector2 (0, 5) * force, ForceMode.Impulse);
				AddMyTouque();
				save_data.objectid++;

			}

		}
		else if (id == 4)
		{//joker
			if (!save_data.active_combo) 
			{
				if (RandomVal < 50)
				{
					transform.position = new Vector3 (-2, -8,save_data.z_axixs[save_data.z_id]);
					rig2d.AddForce (new Vector2 (1, 5) * force, ForceMode.Impulse);


				}
				else 
				{
					transform.position = new Vector3 (2, -8, save_data.z_axixs[save_data.z_id]);
					rig2d.AddForce (new Vector2 (-1, 5) * force, ForceMode.Impulse);


				}
				AddMyTouque();
				save_data.z_id++;
			}
			else
			{	
				//print (save_data.groupid+" " +save_data.objectid);
				transform.position = save_data.combo [save_data.groupid, save_data.objectid];
				rig2d.AddForce (new Vector2 (0, 5) * force, ForceMode.Impulse);
				AddMyTouque();
				save_data.objectid++;

			}
		}
		else if (id == 5)
		{//halloween
			if (!save_data.active_combo) 
			{
				if (RandomVal < 50) 
				{
					transform.position = new Vector3 (-2, -5, save_data.z_axixs[save_data.z_id]);
					rig2d.AddForce (new Vector2 (-1, 3) * force, ForceMode.Impulse);

				} 
				else 
				{
					transform.position = new Vector3 (2, -5,save_data.z_axixs[save_data.z_id]);
					rig2d.AddForce (new Vector2 (1, 3) * force, ForceMode.Impulse);

				}

				AddMyTouque();
				save_data.z_id++;
			} 
			else 
			{
				//print (save_data.groupid+" " +save_data.objectid);
				transform.position = save_data.combo [save_data.groupid, save_data.objectid];
				rig2d.AddForce (new Vector2 (0, 5) * force, ForceMode.Impulse);
				AddMyTouque();
				save_data.objectid++;

			}
		} 
		else if (id == 6)
		{//casper
			if (!save_data.active_combo) 
			{
				if (RandomVal < 50)
				{
					transform.position = new Vector3 (-10, 1,save_data.z_axixs[save_data.z_id]);
					rig2d.AddForce (new Vector2 (3, 1) * force, ForceMode.Impulse);


				}
				else 
				{
					transform.position = new Vector3 (10, 1,save_data.z_axixs[save_data.z_id]);
					rig2d.AddForce (new Vector2 (-3, 1) *force, ForceMode.Impulse);


				}
				AddMyTouque();
				save_data.z_id++;
			} 
			else
			{
				//print (save_data.groupid+" " +save_data.objectid);	
				transform.position = save_data.combo [save_data.groupid, save_data.objectid];
				rig2d.AddForce (new Vector2 (0, 5) * force, ForceMode.Impulse);
				AddMyTouque();
				save_data.objectid++;

			}
		} 
		else if (id == 7) 
		{//spider
			if (RandomVal < 50) 
			{
				transform.position = new Vector3 (Random.Range (1, 3), 7, 1);
			}
			else 
			{
				transform.position = new Vector3 (Random.Range (-1, -3), 7, 1);
			}
		}
		else if (id == 8) 
		{//lettercubes
			if (RandomVal < 50)
			{
				transform.position = new Vector3 (Random.Range (1, 2), -6, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (Random.Range (1, 2), 4) * force, ForceMode.Impulse);



			} 
			else
			{
				transform.position = new Vector3 (Random.Range (-1, -2), -6, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (Random.Range (-1, -2), 4) * force, ForceMode.Impulse);

			

			}
			AddMyTouque();
			save_data.z_id++;

		}
		else if (id == 9) 
		{//good2
			if (RandomVal < 50) 
			{
				transform.position = new Vector3 (8, -6, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (-3, 3) * force, ForceMode.Impulse);
				//AddMyTouque();


			} 
			else
			{
				transform.position = new Vector3 (-8, -6, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (3, 3) * force, ForceMode.Impulse);
				//AddMyTouque();


			}
			AddMyTouque();
			save_data.z_id++;
		
		} 
		else if (id == 10) 
		{//good3
			if (RandomVal < 50)
			{
				transform.position = new Vector3 (8, -6, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (-3, 4) * force, ForceMode.Impulse);
				//AddMyTouque();


			} 
			else 
			{
				transform.position = new Vector3 (-8, -6, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (3, 4) * force, ForceMode.Impulse);
				//AddMyTouque();


			}

			AddMyTouque();
			save_data.z_id++;

		}
		else if (id == 11)
		{//ball
			if (RandomVal < 50)
			{
				transform.position = new Vector3 (-2, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (1, 4) * force, ForceMode.Impulse);
				//AddMyTouque();


			}
			else 
			{
				transform.position = new Vector3 (2, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (-1, 4) * force, ForceMode.Impulse);
				//AddMyTouque();


			}
			AddMyTouque();
			save_data.z_id++;

		} 
		else if (id == 12) 
		{//stroller 
			if (RandomVal < 50)
			{
				transform.position = new Vector3 (-1, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (1, 5) * force, ForceMode.Impulse);

			} 
			else 
			{
				transform.position = new Vector3 (1, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (-1, 5) * force, ForceMode.Impulse);


			}

			AddMyTouque();
			save_data.z_id++;

		}
		else if (id == 13)
		{//socks
			if (RandomVal < 50) 
			{
				transform.position = new Vector3 (1, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (-2, 5) *force, ForceMode.Impulse);
				//AddMyTouque();


			} 
			else 
			{
				transform.position = new Vector3 (-1, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (2, 5) * force, ForceMode.Impulse);
				//AddMyTouque();


			}

			AddMyTouque();
			save_data.z_id++;

		}
		else if (id == 14)
		{//bib
			if (RandomVal < 50)
			{
				transform.position = new Vector3 (-2, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (2, 5) * force, ForceMode.Impulse);
			//	AddMyTouque();


			}
			else 
			{
				transform.position = new Vector3 (2, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (-2, 5) * force, ForceMode.Impulse);
				//AddMyTouque();


			}

			AddMyTouque();
			save_data.z_id++;

		}
		else if (id == 15)
		{//cloth
			if (RandomVal < 50)
			{
				transform.position = new Vector3 (-1, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (2, 4) * force, ForceMode.Impulse);
				//AddMyTouque();


			}
			else 
			{
				transform.position = new Vector3 (1, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (-2, 4) * force, ForceMode.Impulse);
				//AddMyTouque();


			}
			AddMyTouque();
			save_data.z_id++;

		}
		else if (id == 16)
		{//teddybear
			if (RandomVal < 50)
			{
				transform.position = new Vector3 (0, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (-2, 6) * 2f, ForceMode.Impulse);
				//AddMyTouque();


			} 
			else
			{
				transform.position = new Vector3 (-0, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (2, 6) * 2f, ForceMode.Impulse);
				//AddMyTouque();


			}
			AddMyTouque();
			save_data.z_id++;

		} 
		else if (id == 17) 
		{//bath
			if (RandomVal < 50) 
			{
				transform.position = new Vector3 (4, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (-2, 4) * force, ForceMode.Impulse);
			   //AddMyTouque();


			} 
			else 
			{
				transform.position = new Vector3 (-4, -8,save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (2, 4) * force, ForceMode.Impulse);
			//	AddMyTouque();


			}

			AddMyTouque();
			save_data.z_id++;

		} 
		else if (id == 18)
		{//milkbottle
			if (RandomVal < 50) 
			{
				transform.position = new Vector3 (1, -7, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (0, 5) * force, ForceMode.Impulse);
			//	AddMyTouque();


			} 
			else 
			{
				transform.position = new Vector3 (-1, -7, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (0, 5) * force, ForceMode.Impulse);
			//	AddMyTouque();


			}

			AddMyTouque();
			save_data.z_id++;

		} 
		else if (id == 19)
		{//bawl
			if (RandomVal < 50)
			{
				transform.position = new Vector3 (-2, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (2, 4) * force, ForceMode.Impulse);
				//AddMyTouque();


			}
			else 
			{
				transform.position = new Vector3 (2, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (-2, 4) * force, ForceMode.Impulse);
				//AddMyTouque();


			}

			AddMyTouque();
			save_data.z_id++;

		} 
		else if (id == 20)
		{//duck
			if (RandomVal < 50)
			{
				transform.position = new Vector3 (-1.5f, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (1, 4) * force, ForceMode.Impulse);
				//AddMyTouque();


			}
			else
			{
				transform.position = new Vector3 (1.5f, -8, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (-1, 4) * force, ForceMode.Impulse);
				//AddMyTouque();


			}

			AddMyTouque();
			save_data.z_id++;

		} 
		else if (id == 21) 
		{//bouns1
			if (RandomVal < 50)
			{
				transform.position = new Vector3 (-10, 1, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (3, 1) * 5, ForceMode.Impulse);
				//AddMyTouque();


			} 
			else 
			{
				transform.position = new Vector3 (10, 1, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (-3, 1) * 5, ForceMode.Impulse);
				//AddMyTouque();


			}
			AddMyTouque();
			save_data.z_id++;

		}
		else if (id == 22) 
		{//bouns2
			if (RandomVal < 50) 
			{
				transform.position = new Vector3 (-10, 1, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (3, 1) * 5, ForceMode.Impulse);
				//AddMyTouque();


			} 
			else 
			{
				transform.position = new Vector3 (10, 1, save_data.z_axixs[save_data.z_id]);
				rig2d.AddForce (new Vector2 (-3, 1) * 5, ForceMode.Impulse);
				//AddMyTouque();


			}

			AddMyTouque();
			save_data.z_id++;


		}


		this.gameObject.transform.GetChild(id).gameObject.SetActive(true);
		this.gameObject.transform.GetChild(id).gameObject.tag = item.objectTag;
		this.gameObject.transform.GetChild(id).GetComponent<Collider>().isTrigger = true;

		if(save_data.objectid >= 4)
		{
			save_data.objectid = 0;
		}

		if (save_data.z_id >= 4) 
		{

			save_data.z_id = 0;
		}

	}

	public void footerMethod()
	{

	}
	public void reset()
	{
		print ("reset()");

		transform.position = new Vector3(-15, 0, 0);

		rig2d.isKinematic = true;
		//Srenderer.sprite = null;
		//	print(" id =" + idd);

		for (int i = 0; i < 23; i++)
		{
			this.gameObject.transform.GetChild(i).gameObject.SetActive(false);
			this.gameObject.transform.GetChild(i).gameObject.tag = "Untagged";
			this.gameObject.transform.GetChild(i).GetComponent<BoxCollider>().isTrigger = false;
		}
		n.en(this.gameObject);

	}


}
