﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Main : MonoBehaviour {

	public GameObject mainmenu;
	public GameObject select_panel;
	public GameObject facebook_panel;
	public GameObject achievement_panel;
	public GameObject setting_panel;
	public GameObject exit_panel;
	public GameObject help_panel1,help_panel2;


	public Scrollbar baby_scrollbar;
	public Scrollbar background_scrollbar;

	public Slider SFX_volume_slider;
	public Slider music_volume_slider;

	public GameObject menuFX;

	// Use this for initialization
	void Start () 
	{
		save_data.game_over = false;
		Time.timeScale = 1;

        //MARK: - Saltha Create 
        //Init Music and Sound Value
        if(PlayerPrefs.HasKey("Music Volume") && PlayerPrefs.HasKey("Sound Volume"))
        {
            music_volume_slider.value = PlayerPrefs.GetFloat("Music Volume");
            SFX_volume_slider.value = PlayerPrefs.GetFloat("Sound Volume");
        }
        else
        {
            PlayerPrefs.SetFloat("Music Volume", 0.5f);
            PlayerPrefs.SetFloat("Sound Volume", 0.5f);
           
        }
	}

	// Update is called once per frame
	void Update () {
		
		if(save_data.replay_btn_id)
		{
			mainmenu.SetActive (false);
			select_panel.SetActive (true);

			save_data.replay_btn_id = false;
		}
	}

	public void SelectMenu()
	{
		// Application.LoadLevel(1);
		menuFX.SetActive(true);
		Invoke ("SelectMenu2", 0.25f);

	}  

	void SelectMenu2()
	{
		menuFX.SetActive (false);
		select_panel.SetActive (true);
	}
	public void FacebookLogin()
	{
		facebook_panel.SetActive (true);
	} 

	public void achievementpanel()
	{
		achievement_panel.SetActive (true);
	} 


	public void SettingPanel()
	{
		setting_panel.SetActive (true);
	} 

    //MARK: - Salitha Create
    public void SettingPanelCloseBtn()
    {
        PlayerPrefs.SetFloat("Music Volume", music_volume_slider.value);
        PlayerPrefs.SetFloat("Sound Volume", SFX_volume_slider.value);
    }

	public void ExitPanel()
	{
		exit_panel.SetActive (true);
	} 


	public void LoadGame()
	{
		// Application.LoadLevel(1);
		SceneManager.LoadSceneAsync("Game");

	}  

	public void LoadMenu()
	{
		SceneManager.LoadSceneAsync("crappy_nappy");
	}


	public void Slider()
	{
		SFX_volume_slider.value = 0.5f;
		music_volume_slider.value = 0.5f;
	}

	public void OnBabySnappingEnd(int _page)
	{
		switch (_page) {
		case 0: save_data.baby_id = 5;
			break; 
		case 1: save_data.baby_id = 6;
			break;
		case 2: save_data.baby_id = 2;
			break;
		case 3: save_data.baby_id = 0;
			break;
		case 4: save_data.baby_id = 8;
			break;
		case 5: save_data.baby_id = 4;
			break;
		case 6: save_data.baby_id = 7;
			break;
		case 7: save_data.baby_id = 3;
			break;
		case 8: save_data.baby_id = 1;
			break;
		case 9: save_data.baby_id = 9;
			break;
		}
	}

	private int m_BackgroundSelected = 0;
	public void OnBackgroundSnappingEnd(int _page)
	{
		save_data.back_id = _page + 1;
	}

	public void OnRedPlayButtonClicked()
	{
		menuFX.SetActive (false);
		select_panel.SetActive (false);
		help_panel1.SetActive (true);
	}

	public void Help1()
	{
		menuFX.SetActive (true);
		Invoke ("Help1_2", 0.25f);
	}

	void Help1_2()
	{
		menuFX.SetActive (false);
		help_panel1.SetActive (false);
		help_panel2.SetActive (true);
	}

//	void baby_selection2()
//	{
//		menuFX.SetActive (false);
//		select_panel.SetActive (false);
//		help_panel1.SetActive (true);
//	}
//
//	public SpriteRenderer tmp1, tmp2;
//	public void baby_selection()
//	{
//		menuFX.SetActive (true);
//		tmp1.sortingOrder = 0;
//		tmp2.sortingOrder = 0;
//		Invoke ("baby_selection2", 0.25f);
//
//		if (baby_scrollbar.value >= 0f && baby_scrollbar.value <= 0.06419723f)
//		{
//			print ("1 baby");
//			save_data.baby_id = 5;
//
//		}
//		else if (baby_scrollbar.value >=  0.06419724f && baby_scrollbar.value <= 0.2128599f)
//		{
//			print ("2 baby");
//			save_data.baby_id = 6;
//		}
//		else if (baby_scrollbar.value >= 0.2128600f && baby_scrollbar.value <= 0.3559094f)
//		{
//			print ("3 baby");
//			save_data.baby_id = 2;
//		}
//		else if (baby_scrollbar.value >=0.3559095f && baby_scrollbar.value <= 0.4960927f)
//		{
//			print ("4 baby");
//			save_data.baby_id = 0;
//		}
//		else if (baby_scrollbar.value >= 0.4960928f && baby_scrollbar.value <= 0.632585f)
//		{
//			print ("5 baby");
//			save_data.baby_id = 4;
//		}
//		else if (baby_scrollbar.value >= 0.632586f && baby_scrollbar.value <= 0.777427f)
//		{
//			print ("6 baby");
//			save_data.baby_id = 7;
//		}
//		else if (baby_scrollbar.value >= 0.777428f && baby_scrollbar.value <= 0.9124154f)
//		{
//			print ("7 baby");
//			save_data.baby_id = 3;
//		}
//		else if (baby_scrollbar.value >= 0.9124155f && baby_scrollbar.value <=1f)
//		{
//			print ("8 baby");
//			save_data.baby_id = 1;
//		}
//
////		else if (baby_scrollbar.value >= 0.7755856f && baby_scrollbar.value <= 0.9148014f)
////		{
////			//print ("7 baby");
////			//save_data.baby_id = 3;
////			baby_scrollbar.value = 0;
////		}
////		else if (baby_scrollbar.value >= 0.9148015f && baby_scrollbar.value <= 1f)
////		{
////			//print ("8 baby");
////			//save_data.baby_id = 1;
////			baby_scrollbar.value = 0;
////		}
//
//	}
//	public void background_selection()
//	{
//		if (background_scrollbar.value >= 0f && background_scrollbar.value <=0.187166f) 
//		{
//			print ("1 back");
//			save_data.back_id = 1;
//		}
//		else if (background_scrollbar.value >= 0.187167f && background_scrollbar.value <= 0.7748191f) 
//		{
//			print ("2 back");
//			save_data.back_id = 2;
//		}
//		else if (background_scrollbar.value >= 0.7748192f && background_scrollbar.value <= 1f) 
//		{
//			print ("3 back");
//			save_data.back_id = 3;
//		}
////		else if (background_scrollbar.value >= 0.3523900f && background_scrollbar.value <= 0.4951988f) 
////		{
////			print ("4 back");
////			save_data.back_id = 4;
////		}
////		else if (background_scrollbar.value >= 0.4951989f && background_scrollbar.value <= 0.6372705f) 
////		{
////			print ("5 back");
////			save_data.back_id = 5;
////		}
////		else if (background_scrollbar.value >= 0.6372706f && background_scrollbar.value <=  0.7791734f) 
////		{
////			print ("6 back");
////			save_data.back_id = 6;
////		}
////		else if (background_scrollbar.value >= 0.7791735f && background_scrollbar.value <= 0.920166f) 
////		{
////			print ("7 back");
////			save_data.back_id = 7;
////		}
////		else if (background_scrollbar.value >= 0.920167f && background_scrollbar.value <= 1f) 
////		{
////			print ("8 back");
////			save_data.back_id = 8;
////		}
//
//
//	}

	public void Exit()
	{
		Application.Quit ();

	}


	public void Backbtn()
	{
//		menuFX.SetActive (true);
//		tmp1.sortingOrder = 0;
//		tmp2.sortingOrder = 0;
//		Invoke ("Backbtn2", 0.25f);

		menuFX.SetActive (false);
		select_panel.SetActive (false);

	}

//	void Backbtn2()
//	{
//		menuFX.SetActive (false);
//		select_panel.SetActive (false);
//		tmp1.sortingOrder = 1;
//		tmp2.sortingOrder = 1;
//	}
}
