﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestMode : MonoBehaviour {

	public Text btnText;

	public GameObject[] GoodObjectPrefabs;
	public GameObject[] BadObjectPrefabs;
	public GameObject BonusObjectPrefab;

	//Good Objects
	public Queue<GameObject> GoodObject_0 = new Queue<GameObject> ();
	public Queue<GameObject> GoodObject_1 = new Queue<GameObject> ();
	public Queue<GameObject> GoodObject_2 = new Queue<GameObject> ();
	public Queue<GameObject> GoodObject_3 = new Queue<GameObject> ();
	public Queue<GameObject> GoodObject_4 = new Queue<GameObject> ();
	public Queue<GameObject> GoodObject_5 = new Queue<GameObject> ();
	public Queue<GameObject> GoodObject_6 = new Queue<GameObject> ();
	public Queue<GameObject> GoodObject_7 = new Queue<GameObject> ();

	//Bad Objects
	public Queue<GameObject> BadObject_0 = new Queue<GameObject> ();
	public Queue<GameObject> BadObject_1 = new Queue<GameObject> ();
	public Queue<GameObject> BadObject_2 = new Queue<GameObject> ();
	public Queue<GameObject> BadObject_3 = new Queue<GameObject> ();
	public Queue<GameObject> BadObject_4 = new Queue<GameObject> ();
	public Queue<GameObject> BadObject_5 = new Queue<GameObject> ();
	public Queue<GameObject> BadObject_6 = new Queue<GameObject> ();
	public Queue<GameObject> BadObject_7 = new Queue<GameObject> ();

	//Bonus Objects
	public Queue<GameObject> BonusObject = new Queue<GameObject> ();

	//GLDPfx
	public Queue<GameObject> GLDPFXQueue = new Queue<GameObject> ();

	private GameObject m_TempObject;
	private TestItems m_TempTestItem;

	private bool m_PatternIsStillRunning = false;
	private int m_PatternCount = 0;

	private string m_CoroutineToBeCalled = "";

	private int m_GoldenRattleFrequency = 7;
	private int m_GoldenRattleTimer = 0;
	private int m_LargeGroupFrequency = 4;
	private int m_RandomMethodCallCount = 0;

	private int m_ResetItemCountInPattern = 0;
	public int ResetItemCountInPattern
	{
		get { return m_ResetItemCountInPattern; }
		set { m_ResetItemCountInPattern = value; }
	}

	//Patterns to be called during the Golden Time 
	// 2, 3, 6, 11

	// Use this for initialization
	void Start () { 

		m_PerPatternCountDic.Add ("InitGen3_P1", 10);
		m_PerPatternCountDic.Add ("InitGen3_P5", 10);
		m_PerPatternCountDic.Add ("InitGen3_P7", 8);
		m_PerPatternCountDic.Add ("InitGen3_P9", 3);
		m_PerPatternCountDic.Add ("InitGen3_P11", 6);
		m_PerPatternCountDic.Add ("InitGen3_P12", 6);
		m_PerPatternCountDic.Add ("InitGen3_P13", 10);
		m_PerPatternCountDic.Add ("InitGen3_P20", 18);
		m_PerPatternCountDic.Add ("InitGen2_P3", 12);
		m_PerPatternCountDic.Add ("InitGen2_P4", 14);
		m_PerPatternCountDic.Add ("InitGen2_P15", 19);
		m_PerPatternCountDic.Add ("InitGen2_P18", 55);
		m_PerPatternCountDic.Add ("InitPattern1", 2);
		m_PerPatternCountDic.Add ("InitPattern2", 3);
		m_PerPatternCountDic.Add ("InitPattern3", 4);
		m_PerPatternCountDic.Add ("InitPattern4", 3);
		m_PerPatternCountDic.Add ("InitPattern5", 4);
		m_PerPatternCountDic.Add ("InitPattern6", 7);
		m_PerPatternCountDic.Add ("InitPattern7", 12);
		m_PerPatternCountDic.Add ("InitPattern8", 7);
		m_PerPatternCountDic.Add ("InitPattern9", 8);
		m_PerPatternCountDic.Add ("InitPattern10", 8);
		m_PerPatternCountDic.Add ("InitPattern11", 7);
		m_PerPatternCountDic.Add ("InitPattern12", 9);
		m_PerPatternCountDic.Add ("InitPattern13", 2);
		m_PerPatternCountDic.Add ("InitPattern14", 2);
		m_PerPatternCountDic.Add ("InitPattern15", 6);
		m_PerPatternCountDic.Add ("InitPattern16", 7);
		m_PerPatternCountDic.Add ("InitPattern17", 11);
		m_PerPatternCountDic.Add ("InitPattern18", 6);
		m_PerPatternCountDic.Add ("InitPattern19", 13);
		m_PerPatternCountDic.Add ("InitPattern20", 11);
		m_PerPatternCountDic.Add ("InitPattern21", 5);
		m_PerPatternCountDic.Add ("InitPattern22", 5);
		m_PerPatternCountDic.Add ("InitPattern23", 8);
		m_PerPatternCountDic.Add ("InitPattern24", 7);
		m_PerPatternCountDic.Add ("InitPattern25", 9);
		m_PerPatternCountDic.Add ("InitPattern26", 9);
		m_PerPatternCountDic.Add ("InitPattern27", 8);
		m_PerPatternCountDic.Add ("InitPattern28", 7);

		m_GameSpeedState = GameSpeedStates.Normal;
	}

	private int m_ItemCountInPattern = -1;
	private Dictionary<string, int> m_PerPatternCountDic = new Dictionary<string, int> ();
	private int GetPatternItemCount(string _functionName)
	{
		int m_IteCntInPattern;
		if (m_PerPatternCountDic.TryGetValue (_functionName, out m_IteCntInPattern)) {
			return m_IteCntInPattern;
		} else {
			return -1;
		}
	}

	enum GameSpeedStates
	{
		Normal,
		First,
		Second,
		Third,
		Final
	};
	private GameSpeedStates m_GameSpeedState;

	// Update is called once per frame
	void Update () {

		if (bGameOver) {
				Debug.Log ("Game Over");
			enabled = false;
			return;
		}

		if (m_GameSpeedState == GameSpeedStates.Normal && m_PatternCount > 20) {
			Time.timeScale = 1.0425f;
			m_GameSpeedState = GameSpeedStates.First;
		}
		else if(m_GameSpeedState == GameSpeedStates.First && m_PatternCount > 40) {
			Time.timeScale = 1.085f;
			m_TimeGap = m_TimeGap - 0.25f;
			m_GameSpeedState = GameSpeedStates.Second;
		}
		else if(m_GameSpeedState == GameSpeedStates.Second && m_PatternCount > 60) {
			Time.timeScale = 1.12f;
			m_TimeGap = m_TimeGap - 0.25f;
			m_GameSpeedState = GameSpeedStates.Third;
		}
		else if(m_GameSpeedState == GameSpeedStates.Third && m_PatternCount > 80) {
			Time.timeScale = 1.17f;
			m_TimeGap = m_TimeGap - 0.25f;
			m_GameSpeedState = GameSpeedStates.Final;
		}

		if (!m_PatternIsStillRunning) {
			m_PatternIsStillRunning = true;

			if (m_PatternCount == 0) {
				m_PatternCount++;
				btnText.text = "IniP = 1";
				StartCoroutine (InitialPattern1 ());
			} else if (m_PatternCount == 1) {
				m_PatternCount++;
				btnText.text = "IniP = 2";
				StartCoroutine (InitialPattern2 ());
			} else if (m_PatternCount == 2) {
				m_PatternCount++;
				btnText.text = "IniP = 3";
				StartCoroutine (InitialPattern3 ());
			} else {
				m_PatternCount++;
				m_RandomMethodCallCount++;

				if (m_RandomMethodCallCount >= 4 || TrailPointScript.bGldnTm) { // if it is time to call a large function or golden item has being collected
					m_CoroutineToBeCalled = LargeFuncs [Random.Range (0, 15)];
					m_RandomMethodCallCount = 0;
				} else {
					m_CoroutineToBeCalled = AllFuncs [Random.Range (0, 39)];
				}

				if (TrailPointScript.bGldnTm && m_GoldenRattleTimer >= m_GoldenRattleFrequency) {
					m_GoldenRattleTimer = 0;
				}
				else if (m_GoldenRattleTimer >= m_GoldenRattleFrequency) { // It is time for Golden Rattle
					m_GoldenRattleTimer = 0;

					btnText.text = "Gol : " + m_CoroutineToBeCalled;

					InitiateGoldenRattle ();

				} else {
					btnText.text = "Ran: " + m_CoroutineToBeCalled;

				}

				m_ItemCountInPattern = GetPatternItemCount (m_CoroutineToBeCalled);

				ResetItemCountInPattern = 0;

				if (m_ItemCountInPattern == -1)
					Debug.LogError ("This can not be");

				StartCoroutine(m_CoroutineToBeCalled);

				m_GoldenRattleTimer++;
			}

//			ResetItemCountInPattern = 0;
//			m_ItemCountInPattern = GetPatternItemCount ("InitGen2_P19");
//			StartCoroutine ("InitGen2_P19");


		}

		//if (list [t] == "InitPattern7")
			//Debug.Log (" Reset Count " + ResetItemCountInPattern);
			
		if (m_ItemCountInPattern == ResetItemCountInPattern) {

			m_ItemCountInPattern = -1;
			ResetItemCountInPattern = 0;
			m_PatternIsStillRunning = false;
			StopAllCoroutines ();
		}
	}

	private int m_RandomMaxRoundCount;
	private float m_TimeGap = 1;

	private void InitiateGoldenRattle()
	{
		int goCode = Random.Range (0, 5);

		switch (goCode) {
		case 0:
			SelectObject (1, new Vector3(-11.16f,-7.39f,-40f), Quaternion.Euler(0,0,41.506f), new Vector3 (1000f, 0, 0.8f), 3, "", 0, false, true);
			break;
		case 1:
			SelectObject (1, new Vector3(0.18f, -8.22f, -40f), Quaternion.Euler(0,0,90), new Vector3 (1000f, 0, 0.8f), 3, "", 0, false, true);
			break;
		case 2:
			SelectObject (1, new Vector3(-12.95f, -0.54f, -40f), Quaternion.Euler(0,0,0), new Vector3 (1000f, 0, 0.8f), 3, "", 0, false, true);
			break;
		case 3:
			SelectObject (1, new Vector3(11.49f,-3.55f,-40f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 0.8f), 3, "", 0, false, true);
			break;
		case 4:
			SelectObject (1, new Vector3(0f,9.281f,-40f), Quaternion.Euler(0,0,-90), new Vector3 (1000f, 0, 0.8f), 3, "", 0, false, true);
			break;
		}
	}

	int modeCnt=0;
	int easyCnt=0;
	int hardCnt=0;
	int harderCnt=0;
	public static bool bGameOver = false;

	int rndmCnt=0;
	int goldCnt=0;

	string[] AllFuncs = new string[]{	"InitGen3_P1",
										"InitGen3_P5",
										"InitGen3_P7",
		//"InitGen3_P8", Removed becuase InitGen3_P7() will be calling this
										"InitGen3_P9",
										"InitGen3_P11",
										"InitGen3_P12",
										"InitGen3_P13",
										"InitGen3_P20",
		//"InitGen3_P14", Removed becuase InitGen3_P13() will be calling this
		//"InitGen3_P20", , Removed becuase InitGen2_P19() will be calling this
		//"InitGen3_P21", , Removed becuase InitGen3_P20() will be calling this

		//"InitGen2_P2",  Removed because InitGen3_P1() will be calling this
										"InitGen2_P3",
										"InitGen2_P4",
										"InitGen2_P15",
										"InitGen2_P18",
		//"InitGen2_P19",  Removed because InitGen2_P18() will be calling this

										"InitPattern1",
										"InitPattern2",
										"InitPattern3",
										"InitPattern4",
										"InitPattern5",
										"InitPattern6",
										"InitPattern7",
										"InitPattern8",
										"InitPattern9",
										"InitPattern10",
										"InitPattern11",
										"InitPattern12",
										"InitPattern13",
										"InitPattern14",
										"InitPattern15",
										"InitPattern16",
										"InitPattern17",
										"InitPattern18",
										"InitPattern19",
										"InitPattern20",
										"InitPattern21",
										"InitPattern22",
										"InitPattern23",
										"InitPattern24",
										"InitPattern25",
										"InitPattern26",
										"InitPattern27",
										"InitPattern28"

														}; // 40

	string[] LargeFuncs = new string[]{		//"InitGen3_P6", Removed becuase InitGen3_P5() will be calling this
		//"InitGen3_P16", Removed becuase InitGen2_P15() will be calling this
		//"InitGen2_P2", Removed because InitGen3_P1() will be calling this
											"InitGen2_P3",
											"InitGen2_P4",
											"InitGen2_P15",
											"InitGen2_P18",
											"InitGen3_P9",
		//"InitGen2_P19", Removed because InitGen2_P18() will be calling this
											"InitPattern22",
											"InitPattern16",
											"InitPattern9",
											"InitPattern27",
											"InitPattern22",
											"InitPattern21",
											"InitPattern23",
											"InitPattern25",
											"InitPattern28",
											"InitPattern26",

															};


	private int m_iSwap, m_RandomItemIndex;
	private void SelectObject(int _choosedType, Vector3 _startPos, Quaternion _startRot, Vector3 _forceDirection, float _force, string _strPathName, float _f3rdgenSpeed, bool _b3rgGen, bool _goStraight)
	{
		m_iSwap = Random.Range (0, 2);

		m_RandomItemIndex = Random.Range (0, 7);

		if (_choosedType == 0) { // Good Item

			if (m_iSwap == 0) { // choose Good Itself
				FindARandomGoodObject (m_RandomItemIndex);
				m_TempTestItem = m_TempObject.GetComponent<TestItems> ();
			} else { // choose Bad
				FindARandomBadObject(m_RandomItemIndex);
				m_TempTestItem = m_TempObject.GetComponent<TestItems> ();
				_choosedType = 2; // change the choosen type to Bad
			}

			m_TempTestItem.InitiateObject (_choosedType, m_RandomItemIndex, this);
			
		} else if (_choosedType == 1) { // Bonus
			if (BonusObject.Count == 0) {
				m_TempObject = (GameObject)Instantiate (BonusObjectPrefab, m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = BonusObject.Dequeue () as GameObject;
			}
			m_TempObject.SetActive (true);
			m_TempTestItem = m_TempObject.GetComponent<TestItems> ();
			m_TempTestItem.InitiateObject (_choosedType, -1, this);
		} else { // Bad
			if (m_iSwap == 0) { // choose Bad Itself
				FindARandomBadObject (m_RandomItemIndex);
				m_TempTestItem = m_TempObject.GetComponent<TestItems> ();
			} else { // choose Bad
				FindARandomGoodObject(m_RandomItemIndex);
				m_TempTestItem = m_TempObject.GetComponent<TestItems> ();
				_choosedType = 0; // change the choosen type to Good
			}
			m_TempTestItem.InitiateObject (_choosedType, m_RandomItemIndex, this);
		}

		m_TempObject.transform.position = _startPos;
		m_TempObject.transform.rotation = _startRot;

		if (_forceDirection.x == -1000f) {
			m_TempTestItem.forceDir = -m_TempObject.transform.right * _forceDirection.z;
		}
		else if(_forceDirection.x == 1000f) {
			m_TempTestItem.forceDir = m_TempObject.transform.right * _forceDirection.z;
		}
		else {
			m_TempTestItem.forceDir = _forceDirection;
		}

		m_TempTestItem.force = _force;
		m_TempTestItem.strPathName = _strPathName;
		m_TempTestItem.f3rdgenSpd = _f3rdgenSpeed;
		m_TempTestItem.b3rgGen = _b3rgGen;
		m_TempTestItem.goStraight = _goStraight;

		m_TempTestItem.StartMyBehaviours ();

		if (!System.String.IsNullOrEmpty (_strPathName)) {
			m_TempTestItem.GOGO ();
		}
	}

	private Vector3 m_DefaultInstantiatingPos = new Vector3(-1000f, -1000f, 0);
	private void FindARandomGoodObject(int _randomIndex)
	{
		switch (_randomIndex) {
		case 0: 
			if (GoodObject_0.Count == 0) {
				m_TempObject = (GameObject)Instantiate (GoodObjectPrefabs [0], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = GoodObject_0.Dequeue () as GameObject;
			}
			break;
		case 1: 
			if (GoodObject_1.Count == 0) {
				m_TempObject = (GameObject)Instantiate (GoodObjectPrefabs [1], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = GoodObject_1.Dequeue () as GameObject;
			}
			break;
		case 2: 
			if (GoodObject_2.Count == 0) {
				m_TempObject = (GameObject)Instantiate (GoodObjectPrefabs [2], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = GoodObject_2.Dequeue () as GameObject;
			}
			break;
		case 3: 
			if (GoodObject_3.Count == 0) {
				m_TempObject = (GameObject)Instantiate (GoodObjectPrefabs [3], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = GoodObject_3.Dequeue () as GameObject;
			}
			break;
		case 4: 
			if (GoodObject_4.Count == 0) {
				m_TempObject = (GameObject)Instantiate (GoodObjectPrefabs [4], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = GoodObject_4.Dequeue () as GameObject;
			}
			break;
		case 5: 
			if (GoodObject_5.Count == 0) {
				m_TempObject = (GameObject)Instantiate (GoodObjectPrefabs [5], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = GoodObject_5.Dequeue () as GameObject;
			}
			break;
		case 6: 
			if (GoodObject_6.Count == 0) {
				m_TempObject = (GameObject)Instantiate (GoodObjectPrefabs [6], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = GoodObject_6.Dequeue () as GameObject;
			}
			break;
		case 7: 
			if (GoodObject_7.Count == 0) {
				m_TempObject = (GameObject)Instantiate (GoodObjectPrefabs [7], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = GoodObject_7.Dequeue () as GameObject;
			}
			break;
		default:
			m_TempObject = null;
			break;
		}
	}

	private void FindARandomBadObject(int _randomIndex)
	{
		switch (_randomIndex) {
		case 0: 
			if (BadObject_0.Count == 0) {
				m_TempObject = (GameObject)Instantiate (BadObjectPrefabs [0], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = BadObject_0.Dequeue () as GameObject;
			}
			break;
		case 1: 
			if (BadObject_1.Count == 0) {
				m_TempObject = (GameObject)Instantiate (BadObjectPrefabs [1], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = BadObject_1.Dequeue () as GameObject;
			}
			break;
		case 2: 
			if (BadObject_2.Count == 0) {
				m_TempObject = (GameObject)Instantiate (BadObjectPrefabs [2], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = BadObject_2.Dequeue () as GameObject;
			}
			break;
		case 3: 
			if (BadObject_3.Count == 0) {
				m_TempObject = (GameObject)Instantiate (BadObjectPrefabs [3], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = BadObject_3.Dequeue () as GameObject;
			}
			break;
		case 4: 
			if (BadObject_4.Count == 0) {
				m_TempObject = (GameObject)Instantiate (BadObjectPrefabs [4], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = BadObject_4.Dequeue () as GameObject;
			}
			break;
		case 5: 
			if (BadObject_5.Count == 0) {
				m_TempObject = (GameObject)Instantiate (BadObjectPrefabs [5], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = BadObject_5.Dequeue () as GameObject;
			}
			break;
		case 6: 
			if (BadObject_6.Count == 0) {
				m_TempObject = (GameObject)Instantiate (BadObjectPrefabs [6], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = BadObject_6.Dequeue () as GameObject;
			}
			break;
		case 7: 
			if (BadObject_7.Count == 0) {
				m_TempObject = (GameObject)Instantiate (BadObjectPrefabs [7], m_DefaultInstantiatingPos, Quaternion.identity);
			} else {
				m_TempObject = BadObject_7.Dequeue () as GameObject;
			}
			break;
		default:
			m_TempObject = null;
			break;
		}
	}

	// 0 = Good
	// 1 = Bonus
	// 2 = Bad


	IEnumerator InitialPattern1()
	{
		SelectObject (2, new Vector3 (11.42f, -8.01f, 0.42f), Quaternion.Euler (0, 0, 22.45f), new Vector3 (-2, 4.5f, 0) * 1.03f, 3, "", 0, false, false);

		yield return new WaitForSeconds (0.55f * m_TimeGap);

		SelectObject (0, new Vector3(-11.52f,-8.22f,-4.98f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 4.5f, 0) * 1.03f, 3, "", 0, false, false);

		yield return new WaitForSeconds (2);

		m_PatternIsStillRunning = false;
	}

	IEnumerator InitialPattern2()
	{
		SelectObject (0, new Vector3(-11.52f,-8.22f,-4.98f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 4.5f, 0)* 1.03f, 3, "", 0, false, false);

		yield return new WaitForSeconds (0.61f * m_TimeGap);

		SelectObject (2, new Vector3(5.69f,-7.62f,0.42f), Quaternion.Euler(0,0,131.367f), new Vector3 (-2, 4.5f, 0) * 1.03f, 3, "", 0, false, false);

		yield return new WaitForSeconds (1 * m_TimeGap);

		SelectObject (2, new Vector3(5.16f,8.2f,-8.81f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, -2.5f, 0), 3, "", 0, false, false);

		yield return new WaitForSeconds (2);

		m_PatternIsStillRunning = false;
	}

	IEnumerator InitialPattern3()
	{
		SelectObject (0, new Vector3(-7.44f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 4.8f, 0) * 1.02f, 3, "", 0, false, false);

		yield return new WaitForSeconds (1.07f * m_TimeGap);

		SelectObject (2, new Vector3(3.23f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 4f, 0) * 1.02f, 3, "", 0, false, false);

		yield return new WaitForSeconds (1.02f * m_TimeGap);
		SelectObject (0, new Vector3(7.63f,8.2f,-8.81f), Quaternion.Euler(0,0,0), new Vector3 (0, -2.5f, 0), 1.47f, "", 0, false, false);

		yield return new WaitForSeconds (0.83f * m_TimeGap);

		SelectObject (2, new Vector3(-1.75f,8.2f,-8.81f), Quaternion.Euler(0,0,0), new Vector3 (0, -2.5f, 0), 3f, "", 0, false, false);

		yield return new WaitForSeconds (2);
		m_PatternIsStillRunning = false;
	}

	IEnumerator InitPattern1(){
		

		SelectObject (2, new Vector3 (11.42f, -8.01f, 0.42f), Quaternion.Euler (0, 0, 22.45f), new Vector3 (-2, 4.5f, 0) * 1.03f, 3, "", 0, false, false);

		yield return new WaitForSeconds (0.55f * m_TimeGap);

		SelectObject (0, new Vector3(-11.52f,-8.22f,-4.98f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 4.5f, 0) * 1.03f, 3, "", 0, false, false);

	}


	IEnumerator InitPattern2(){
		

		SelectObject (0, new Vector3(-11.52f,-8.22f,-4.98f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 4.5f, 0)* 1.03f, 3, "", 0, false, false);

		yield return new WaitForSeconds (0.61f * m_TimeGap);//sldr_gap1n2.value);//0.2

		SelectObject (2, new Vector3(5.69f,-7.62f,0.42f), Quaternion.Euler(0,0,131.367f), new Vector3 (-2, 4.5f, 0) * 1.03f, 3, "", 0, false, false);

		yield return new WaitForSeconds (1 * m_TimeGap);//sldr_gap2n3.value);//1

		SelectObject (2, new Vector3(5.16f,8.2f,-8.81f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, -2.5f, 0), 3, "", 0, false, false);

	}

	IEnumerator InitPattern3(){

		SelectObject (0, new Vector3(-7.44f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 4.8f, 0) * 1.02f, 3, "", 0, false, false);

		yield return new WaitForSeconds (1.07f * m_TimeGap);

		SelectObject (2, new Vector3(3.23f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 4f, 0) * 1.02f, 3, "", 0, false, false);

		yield return new WaitForSeconds (1.02f * m_TimeGap);

		SelectObject (0, new Vector3(7.63f,8.2f,-8.81f), Quaternion.Euler(0,0,0), new Vector3 (0, -2.5f, 0), 1.47f, "", 0, false, false);

		yield return new WaitForSeconds (0.83f * m_TimeGap);

		SelectObject (2, new Vector3(-1.75f,8.2f,-8.81f), Quaternion.Euler(0,0,0), new Vector3 (0, -2.5f, 0), 3f, "", 0, false, false);

	}


	IEnumerator InitPattern4(){


		SelectObject (0, new Vector3(-11.52f,-8.22f,-4.98f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 4.5f, 0)*1.02f, 3, "", 0, false, false);

		yield return new WaitForSeconds (1.07f * m_TimeGap);

		SelectObject (0, new Vector3(5.69f,-7.62f,0.42f), Quaternion.Euler(0,0,131.367f), new Vector3 (2, 4.5f, 0)*1.02f, 3, "", 0, false, false);

		yield return new WaitForSeconds (1.02f * m_TimeGap);

		SelectObject (2, new Vector3(-5.91f,8.2f,-8.81f), Quaternion.Euler(0,0,22.45f), new Vector3 (4, -2.5f, 0), 2, "", 0, false, false);

	}


	IEnumerator InitPattern5(){

		SelectObject (0, new Vector3(-11.52f,-8.22f,-4.98f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 4.5f, 0) * 1.02f, 3, "", 0, false, false);

		yield return new WaitForSeconds (1.07f * m_TimeGap);

		SelectObject (0, new Vector3(11.42f,-8.01f,0.42f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, 4.5f, 0)*1.02f, 3, "", 0, false, false);

		yield return new WaitForSeconds (1.42f * m_TimeGap);

		SelectObject (2, new Vector3(10.23f,8.2f,-8.81f), Quaternion.Euler(0,0,22.45f), new Vector3 (-6, -2.5f, 0), 0.48f, "", 0, false, false);

		yield return new WaitForSeconds (0.83f * m_TimeGap);

		SelectObject (2, new Vector3(-11.52f,-4.5f,-10.25f), Quaternion.Euler(0,0,-25.858f), new Vector3 (5, 4f, 0) * 1.01f, 3, "", 0, false, false);

	}

	IEnumerator InitPattern6(){
		
		SelectObject (0, new Vector3(-11.52f,-4.5f,-10.25f), Quaternion.Euler(0,0,-25.858f), new Vector3 (4, 4f, 0) * 1.01f, 3, "", 0, false, false);


		SelectObject (2, new Vector3(-3.9f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 4.0f, 0) * 1.01f, 3, "", 0, false, false);


		SelectObject (0, new Vector3(11.42f,-8.01f,-5.1f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, 4.0f, 0) * 1.01f, 3, "", 0, false, false);

		yield return new WaitForSeconds (1.07f * m_TimeGap);

		SelectObject (2, new Vector3(13.7f,-8.01f,-14f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, 4.0f, 0)*1.01f, 3, "", 0, false, false);

		yield return new WaitForSeconds (1.3f * m_TimeGap);

		SelectObject (2, new Vector3(3.23f,-8.01f,5.7f), Quaternion.Euler(0,0,0f), new Vector3 (0, 5f, 0) * 0.91f, 3, "", 0, false, false);


		SelectObject (0, new Vector3(-11.52f,-8.22f,-4.98f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 4.0f, 0)* 0.91f, 3, "", 0, false, false);

		yield return new WaitForSeconds (1.08f * m_TimeGap);

		SelectObject (0, new Vector3(0.1f,-8.01f,10f), Quaternion.Euler(0,0,0), new Vector3 (0, 4.0f, 0) * 1.04f, 3, "", 0, false, false);
	
	}


	IEnumerator InitPattern7(){

		SelectObject (2, new Vector3(-11.52f,-4.5f,-10.25f), Quaternion.Euler(0,0,-25.858f), new Vector3 (3, 3.5f, 0) * 1.01f, 3, "", 0, false, false);

		SelectObject (0, new Vector3(-11.52f,-8.22f,-0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 3.50f, 0)*1.01f, 3, "", 0, false, false);

		yield return new WaitForSeconds (1.08f * m_TimeGap);

		SelectObject (0, new Vector3(13.7f,-5.01f,-14f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, 3.50f, 0)*1.01f, 3, "", 0, false, false);


		SelectObject (2, new Vector3(11.42f,-8.01f,-5.1f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, 3.50f, 0)*1.01f, 3, "", 0, false, false);

		yield return new WaitForSeconds (1.32f * m_TimeGap);
	
		SelectObject (2, new Vector3(10.23f,8.2f,-8.81f), Quaternion.Euler(0,0,22.45f), new Vector3 (-6, -3.5f, 0), 1.5f, "", 0, false, false);

		yield return new WaitForSeconds (1.54f * m_TimeGap);
	
		SelectObject (0, new Vector3(5.16f,8.2f,4.7584f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, -2.5f, 0), 1f, "", 0, false, false);

		yield return new WaitForSeconds (1.7f * m_TimeGap);

		SelectObject (0, new Vector3 (7.38f, 8.2f, -8.81f), Quaternion.Euler (0, 0, 22.45f), new Vector3 (-2, -2.5f, 0), 1.42f, "", 0, false, false);

	
		SelectObject (2, new Vector3(0.68f,8.2f,-18.81f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, -2.5f, 0), 1.42f, "", 0, false, false);


		SelectObject (0, new Vector3(-2.36f,8.2f,-18.81f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, -2.5f, 0), 1.42f, "", 0, false, false);

		yield return new WaitForSeconds (1.36f * m_TimeGap);

		SelectObject (0, new Vector3(-5.13f,-8.01f,19f), Quaternion.Euler(0,0,0), new Vector3 (0, 4.0f, 0) * 1.1f, 3f, "", 0, false, false);

	
		SelectObject (2, new Vector3(-0.14f,-8.01f,19f), Quaternion.Euler(0,0,0), new Vector3 (0, 4.0f, 0) * 1.1f, 3f, "", 0, false, false);

		SelectObject (0, new Vector3(5.31f,-8.01f,19f), Quaternion.Euler(0,0,0), new Vector3 (0, 4.0f, 0) * 1.1f, 3f, "", 0, false, false);

		yield return null;

	}


	IEnumerator InitPattern8(){

		SelectObject (2, new Vector3(-11.52f,-4.5f,-10.25f), Quaternion.Euler(0,0,-25.858f), new Vector3 (3, 3.5f, 0) * 1.01f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (1.55f * m_TimeGap);

		SelectObject (2, new Vector3(1.66f,-5.01f,-14f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, 3.50f, 0)*1.01f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (1.32f * m_TimeGap);

		SelectObject (2, new Vector3(12.89f,3.59f,-5.29f), Quaternion.Euler(0,0,22.45f), new Vector3 (-18, -2.0f, 0), 1.3f, "", 0, false, false);

		yield return new WaitForSeconds (2.55f * m_TimeGap);

		SelectObject (2, new Vector3(3.72f,7.91f,-8.29f), Quaternion.Euler(0,0,22.45f), new Vector3 (-12, -1.0f, 0), 0.6f, "", 0, false, false);

		yield return new WaitForSeconds (1.72f * m_TimeGap);

		SelectObject (0, new Vector3(-6.15f,-8.22f,-0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 3.750f, 0), 3f, "", 0, false, false);

		SelectObject (0, new Vector3(-2.15f,-8.22f,5.5f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 3.750f, 0), 3f, "", 0, false, false);

		yield return new WaitForSeconds (1.36f * m_TimeGap);

		SelectObject (2, new Vector3(-0.14f,-8.01f,19f), Quaternion.Euler(0,0, 0), new Vector3 (0, 4.0f, 0) * 1.14f, 3f, "", 0, false, false);

	}

	IEnumerator InitPattern9(){

		SelectObject (0, new Vector3(-8.77f,-8.01f,22.22f), Quaternion.Euler(0,0, 0), new Vector3 (0, 5.0f, 0) * 1.07f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-5.64f,-8.01f,15.22f), Quaternion.Euler(0,0, 0), new Vector3 (0, 4.0f, 0) * 1.07f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(-2.77f,-8.01f,22.22f), Quaternion.Euler(0,0, 0), new Vector3 (0, 5.0f, 0) * 1.07f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-0.64f,-8.01f,15.22f), Quaternion.Euler(0,0, 0), new Vector3 (0, 4.5f, 0) * 1.07f, 3f, "", 0, false, false);

		SelectObject (0, new Vector3(2.77f,-8.01f,22.22f), Quaternion.Euler(0,0, 0), new Vector3 (0, 4f, 0) * 1.07f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(4.77f,-8.01f,15.22f), Quaternion.Euler(0,0, 0), new Vector3 (0, 4.5f, 0) * 1.07f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(6.77f,-8.01f,22.22f), Quaternion.Euler(0,0, 0), new Vector3 (0, 4.7f, 0) * 1.07f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(8.77f,-8.01f,10.22f), Quaternion.Euler(0,0, 0), new Vector3 (0, 4f, 0) * 1.07f, 3f, "", 0, false, false);

		save_data.active_combo = true;

		yield return null;

	}

	IEnumerator InitPattern10(){


		SelectObject (0, new Vector3(-11.52f,-8.22f,-0.7f), Quaternion.Euler(0,0, -25.858f), new Vector3 (2, 4f, 0) * 0.92f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(11.42f,-8.01f,-5.1f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, 4f, 0) * 0.92f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (1.05f * m_TimeGap);//sldr_gap1n2.value);


		SelectObject (2, new Vector3(-11.52f,-4.22f,10.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 4f, 0) * 0.92f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(11.42f,-4.01f,-18.1f), Quaternion.Euler(0,0, 22.45f), new Vector3 (-2, 4f, 0) * 0.92f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (1.33f * m_TimeGap);//sldr_gap2n3.value);


		SelectObject (0, new Vector3(-5.64f,-8.01f,15.22f), Quaternion.Euler(0,0, 0), new Vector3 (0, 5f, 0), 3f, "", 0, false, false);

		SelectObject (2, new Vector3(3.4f,8.2f,-8.81f), Quaternion.Euler(0,0,0), new Vector3 (0, -2.5f, 0), 1f, "", 0, false, false);

		yield return new WaitForSeconds (1.55f * m_TimeGap);//sldr_gap3n4.value);


		SelectObject (0, new Vector3(-6.52f,-8.22f,-0.7f), Quaternion.Euler(0,0, -25.858f), new Vector3 (2, 5f, 0) * 0.93f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(6.42f,-8.01f,-5.1f), Quaternion.Euler(0,0, 22.45f), new Vector3 (-2, 5f, 0) * 0.93f, 3f, "", 0, false, false);

	}

	IEnumerator InitPattern11(){

		SelectObject (0, new Vector3(-7.12f,8.07f,-8.81f), Quaternion.Euler(0,0,-73.44701f), new Vector3 (1000f, 0, 1), 3f, "", 0, false, false);

		yield return new WaitForSeconds (0.40f * m_TimeGap);


		SelectObject (0, new Vector3(11.42f,-8.01f,0.42f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, 4.5f, 0), 3f, "", 0, false, false);

		yield return new WaitForSeconds (1.1f * m_TimeGap);


		SelectObject (2, new Vector3(6f,9.6f,-8.81f), Quaternion.Euler(0,0,-103.376f), new Vector3 (1000f, 0, 1), 3f, "", 0, false, true);

		yield return new WaitForSeconds (0.7f * m_TimeGap);


		SelectObject (0, new Vector3(-11.16f,-7.39f,-15.7f), Quaternion.Euler(0,0,41.506f), new Vector3 (1000f, 0, 1.65f), 3f, "", 0, false, true);

		yield return new WaitForSeconds (1.1f * m_TimeGap);


		SelectObject (2, new Vector3(1f,-8.1f,10.7f), Quaternion.Euler(0,0,53.379f), new Vector3 (1000f, 0, 1.35f), 3f, "", 0, false, true);

		yield return new WaitForSeconds (1.1f * m_TimeGap);

		SelectObject (2, new Vector3(0f,-8.01f,0.42f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, 4.5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(0f,9.281f), Quaternion.Euler(0,0,-90f), new Vector3 (1000f, 0, 1), 3f, "", 0, false, true);


	}

	IEnumerator InitPattern12(){


		SelectObject (0, new Vector3(11.49f,2.79f,18.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.85f), 3f, "", 0, false, true);

		yield return new WaitForSeconds (1.65f * m_TimeGap);


		SelectObject (2, new Vector3(-12.95f,-0.54f,-10.7f), Quaternion.Euler(0,0,0), new Vector3 (1000f, 0, 1.85f), 3f, "", 0, false, true);

		yield return new WaitForSeconds (1.85f * m_TimeGap);

		SelectObject (0, new Vector3(11.49f,-3.55f,10.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.8f), 3f, "", 0, false, true);

		yield return new WaitForSeconds (1.08f * m_TimeGap);

		SelectObject (0, new Vector3(-6.91f,-8.01f,0.42f), Quaternion.Euler(0,0,22.45f), new Vector3 (2, 4.5f, 0), 3f, "", 0, false, false);

		yield return new WaitForSeconds (1.02f * m_TimeGap);

		SelectObject (2, new Vector3(-6.91f,8.2f,-8.81f), Quaternion.Euler(0,0,22.45f), new Vector3 (3, -2.5f, 0), 1.2f, "", 0, false, false);


		SelectObject (2, new Vector3(6.36f,-8.22f,10.7f), Quaternion.Euler(0,0,108.576f), new Vector3 (1000f, 0, 1.2f), 3f, "", 0, false, true);

		yield return new WaitForSeconds (1.08f * m_TimeGap);


		SelectObject (0, new Vector3(2.93f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0) * 0.85f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-2.63f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0) * 0.85f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(-6.77f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0) * 0.85f, 3f, "", 0, false, false);


	}


	IEnumerator InitPattern13(){
		
		SelectObject (2, new Vector3(-12.55f,-4.89f,-10.7f), Quaternion.Euler(0,0,23.583f), new Vector3 (1000f, 0f, 1.9f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(12.54f,-4.56f,10.7f), Quaternion.Euler(0,0,155.791f), new Vector3 (1000f, 0f, 1.9f), 3f, "", 0, false, true);

		save_data.active_combo = true;

		yield return null;
	}


	IEnumerator InitPattern14(){

		SelectObject (0, new Vector3(-12.55f,-4.89f,-10.7f), Quaternion.Euler(0,0,23.583f), new Vector3 (1000f, 0f, 1.37f), 3f, "", 0, false, true);


		SelectObject (0, new Vector3(12.54f,-4.56f,10.7f), Quaternion.Euler(0,0,155.791f), new Vector3 (1000f, 0f, 1f), 3f, "", 0, false, true);

		save_data.active_combo = true;

		yield return null;

	}

	IEnumerator InitPattern15(){

		SelectObject (0, new Vector3(-12.95f,3.59f,-15.7f), Quaternion.Euler(0,0,0), new Vector3 (1000f, 0f, 1.28f), 3f, "", 0, false, true);

		SelectObject (2, new Vector3(-12.95f,-0.13f,10.7f), Quaternion.Euler(0,0,0), new Vector3 (1000f, 0f, 1.28f), 3f, "", 0, false, true);

		SelectObject (0, new Vector3(-12.95f,-4.21f,-10.7f), Quaternion.Euler(0,0,0), new Vector3 (1000f, 0f, 1.28f), 3f, "", 0, false, true);

		yield return new WaitForSeconds (1.56f * m_TimeGap);


		SelectObject (0, new Vector3(11.4f,3.59f,10.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0f, 1.87f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(11.4f,-0.13f,-10.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0f, 1.87f), 3f, "", 0, false, true);


		SelectObject (0, new Vector3(11.4f,-4.21f,0.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0f, 1.87f), 3f, "", 0, false, true);

		save_data.active_combo = true;


	}

	IEnumerator InitPattern16(){


		SelectObject (0, new Vector3(-7.81f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 3.4f, 0) * 1.37f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-5.11f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 3.77f, 0)*1.37f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(-2.23f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.14f, 0)*1.37f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(0.18f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.51f, 0)*1.37f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(2.76f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.88f, 0)*1.37f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(5.34f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5.1f, 0)*1.37f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(7.93f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5.25f, 0)*1.37f, 3f, "", 0, false, false);

		save_data.active_combo = true;

		yield return null;
	}


	IEnumerator InitPattern17(){

		SelectObject (2, new Vector3(-10.06f,8.28f,18.81f), Quaternion.Euler(0,0,-53.075f), new Vector3 (1000f, 0, 1.37f), 3f, "", 0, false, true);

		yield return new WaitForSeconds (0.88f * m_TimeGap);

		SelectObject (2, new Vector3(-2.36f,8.92f,18.81f), Quaternion.Euler(0,0,-53.075f), new Vector3 (1000f, 0, 1.37f), 3f, "", 0, false, true);

		yield return new WaitForSeconds (1.1f * m_TimeGap);

		SelectObject (0, new Vector3(-6.07f,8.3f,18.81f), Quaternion.Euler(0,0,-53.075f), new Vector3 (1000f, 0, 1.27f), 3f, "", 0, false, true);

		SelectObject (2, new Vector3(-10.93f,0.16f,-18.42f), Quaternion.Euler(0,0,22.45f), new Vector3 (2, 2f, 0) * 1.27f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (1 * m_TimeGap);

		SelectObject (2, new Vector3(-4.65f,-8.37f,20.42f), Quaternion.Euler(0,0,22.45f), new Vector3 (2, 4f, 0) * 1.28f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (1.4f * m_TimeGap);

		SelectObject (0, new Vector3(-5.11f,-8.22f,-10.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0) * 1.4f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(0.18f,-8.22f,-10.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0) * 1.4f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(11.68f,-0.09f,-10f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, 1f, 0) * 1.4f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (1.1f * m_TimeGap);


		SelectObject (2, new Vector3(4.23f,8.19f,8f), Quaternion.Euler(0,0,-140.388f), new Vector3 (1000f, 0, 1.4f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(8.76f,7.89f,0f), Quaternion.Euler(0,0,-140.388f), new Vector3 (1000f, 0, 1.4f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(11.91f,5.91f,-5f), Quaternion.Euler(0,0,-140.388f), new Vector3 (1000f, 0, 1.4f), 3f, "", 0, false, true);

	}

	IEnumerator InitPattern18(){


		SelectObject (2, new Vector3(-12.3f,-3.8f,0.7f), Quaternion.Euler(0,0,29.026f), new Vector3 (1000f, 0, 1.37f), 3f, "", 0, false, true);


		SelectObject (0, new Vector3(-11.4f,-6.4f,10.7f), Quaternion.Euler(0,0,29.026f), new Vector3 (1000f, 0, 1.37f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(-9f,-8.6f,20.7f), Quaternion.Euler(0,0,29.026f), new Vector3 (1000f, 0, 1.37f), 3f, "", 0, false, true);

		yield return new WaitForSeconds (1.35f * m_TimeGap);//sldr_gap1n2.value);


		SelectObject (2, new Vector3(13.1f,-4.1f,-20.7f), Quaternion.Euler(0,0,141.182f), new Vector3 (1000f, 0, 1.37f), 3f, "", 0, false, true);


		SelectObject (0, new Vector3(11.6f,-6.7f,-10.7f), Quaternion.Euler(0,0,141.182f), new Vector3 (1000f, 0, 1.37f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(8.9f,-8.3f,0.7f), Quaternion.Euler(0,0,141.182f), new Vector3 (1000f, 0, 1.37f), 3f, "", 0, false, true);

	}

	IEnumerator InitPattern19(){


		SelectObject (2, new Vector3(-10.93f,0.16f,0.42f), Quaternion.Euler(0,0,22.45f), new Vector3 (2, 2f, 0), 3f, "", 0, false, false);


		SelectObject (0, new Vector3(-5.11f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0), 3f, "", 0, false, false);


		SelectObject (0, new Vector3(5.32f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(11.68f,-0.09f,-10.42f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, 1f, 0), 3f, "", 0, false, false);

		yield return new WaitForSeconds (1.8f * m_TimeGap);//sldr_gap1n2.value);


		SelectObject (0, new Vector3(-2.11f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0), 3f, "", 0, false, false);


		SelectObject (0, new Vector3(2.32f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-0f,8.2f,-8.81f), Quaternion.Euler(0,0,-90f), new Vector3 (1000f, 0, 1), 3f, "", 0, false, true);

		yield return new WaitForSeconds (1.75f * m_TimeGap);//sldr_gap2n3.value);

		SelectObject (2, new Vector3(6.36f,7.97f,-8.81f), Quaternion.Euler(0,0,-135.639f), new Vector3 (1000f, 0, 1), 3f, "", 0, false, true);


		SelectObject (0, new Vector3(10.06f,7.2f,8.81f), Quaternion.Euler(0,0,-131.814f), new Vector3 (1000f, 0, 1), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(11.92f,4.58f,0.81f), Quaternion.Euler(0,0,-138.138f), new Vector3 (1000f, 0, 1), 3f, "", 0, false, true);

		yield return new WaitForSeconds (1 * m_TimeGap);


		SelectObject (2, new Vector3(-6.48f,7.79f,-18.81f), Quaternion.Euler(0,0,-38.113f), new Vector3 (1000f, 0, 1.8f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(-10.45f,7.06f,8.81f), Quaternion.Euler(0,0,-38.113f), new Vector3 (1000f, 0, 1.8f), 3f, "", 0, false, true);


		SelectObject (0, new Vector3(-12.85f,4f,0f), Quaternion.Euler(0,0,-38.113f), new Vector3 (1000f, 0, 1.8f), 3f, "", 0, false, true);

	}

	IEnumerator InitPattern20(){


		SelectObject (2, new Vector3(-12.87f,-4.41f,-10.7f), Quaternion.Euler(0,0,31.665f), new Vector3 (1000f, 0, 1.8f), 3f, "", 0, false, true);

		yield return new WaitForSeconds (1.25f * m_TimeGap);//sldr_gap1n2.value);


		SelectObject (2, new Vector3(-2.71f,8.27f,-8.81f), Quaternion.Euler(0,0,306.848f), new Vector3 (1000f, 0, 1.8f), 3f, "", 0, false, true);

		yield return new WaitForSeconds (1.25f * m_TimeGap);//sldr_gap2n3.value);

		SelectObject (2, new Vector3(11.26f,8.83f,20.81f), Quaternion.Euler(0,0,232.663f), new Vector3 (1000f, 0, 1.37f), 3f, "", 0, false, true);

		yield return new WaitForSeconds (1 * m_TimeGap);//sldr_gap3n4.value);


		SelectObject (2, new Vector3(0.68f,-8.13f,10.7f), Quaternion.Euler(0,0,127.197f), new Vector3 (1000f, 0, 1.7f), 3f, "", 0, false, true);

		yield return new WaitForSeconds (1.2f * m_TimeGap);//sldr_gap4n5.value);


		SelectObject (2, new Vector3(-7f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0) * 1.50f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-0.67f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0) * 1.50f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(6.44f,-8.22f,10.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0) * 1.50f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (1.25f * m_TimeGap);//sldr_gap5n6.value);


		SelectObject (0, new Vector3(-3.35f,-8.22f,-20f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0) * 0.85f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(2.41f,-8.22f,20f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0) * 0.85f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-7f,8.2f,20f), Quaternion.Euler(0,0,-90f), new Vector3 (1000f, 0, 0.85f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(6.44f,8.2f,20f), Quaternion.Euler(0,0,-90f), new Vector3 (1000f, 0, 0.85f), 3f, "", 0, false, true);
	

	}

	IEnumerator InitPattern21(){


		SelectObject (2, new Vector3(-7.81f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-5.11f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(0.18f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0), 3f, "", 0, false, false);

		SelectObject (2, new Vector3(2.76f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(7.93f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0, 4f, 0), 3f, "", 0, false, false);

		save_data.active_combo = true;

		yield return null;//yield return new WaitForSeconds (1);//yield return null;

	}

	IEnumerator InitPattern22(){


		SelectObject (2, new Vector3(-7.81f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-5.11f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(0.18f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(2.76f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(7.93f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0), 3f, "", 0, false, false);

		save_data.active_combo = true;

		yield return null;//yield return new WaitForSeconds (1);//yield return null;

	}

	IEnumerator InitPattern23(){


		SelectObject (2, new Vector3(-7.81f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-5.11f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0)*1.4f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-1.6f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0)*1.4f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(0f,-8.22f,0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4f, 0)*1.4f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(2.98f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0f)*1.4f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(5.8f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f,5f, 0)*1.4f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(6.81f,-8.22f,0f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4f, 0)*1.4f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(8.4f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0)*1.4f, 3f, "", 0, false, false);

		save_data.active_combo = true;

		yield return new WaitForSeconds (1);//yield return new WaitForSeconds (1);//yield return null;
	

	}

	IEnumerator InitPattern24(){

		SelectObject (2, new Vector3(-7.81f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0)*1.3f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-5.11f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f,4.5f, 0)*1.3f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-1f,-8.22f,0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4f, 0)*1.3f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(0.81f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0)*1.3f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(3.81f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f,3.5f, 0)*1.3f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(5.11f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f,4.5f, 0)*1.3f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(7.81f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4f, 0)*1.3f, 3f, "", 0, false, false);

		save_data.active_combo = true;

		yield return null;
	
	}

	IEnumerator InitPattern25(){


		SelectObject (2, new Vector3(-7.81f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-4.3f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0), 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-0.5f,-8.22f,0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0), 3f, "", 0, false, false);

		yield return new WaitForSeconds (0.4f * m_TimeGap);


		SelectObject (2, new Vector3(2.6f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 3.5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(5.2f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 3.5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(7.9f,-8.22f,0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 3.5f, 0), 3f, "", 0, false, false);

		yield return new WaitForSeconds (1.5f * m_TimeGap);

		SelectObject (2, new Vector3(5.2f,-8.22f,10.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(7.9f,-8.22f,-10.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-2f,-8.22f,5.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0), 3f, "", 0, false, false);

		save_data.active_combo = true;

		yield return null;//yield return new WaitForSeconds (1);//
	
	}

	IEnumerator InitPattern26(){


		SelectObject (0, new Vector3(-7.81f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0), 3f, "", 0, false, false);


		SelectObject (0, new Vector3(-4.18f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0), 3f, "", 0, false, false);


		SelectObject (0, new Vector3(-1.23f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0), 3f, "", 0, false, false);


		SelectObject (0, new Vector3(2.27f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0), 3f, "", 0, false, false);


		SelectObject (0, new Vector3(7.01f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0), 3f, "", 0, false, false);

		yield return new WaitForSeconds (0.4f * m_TimeGap);


		SelectObject (0, new Vector3(-5.96f,-8.22f,0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-3.42f,-8.22f,-5.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4f, 0), 3f, "", 0, false, false);


		SelectObject (0, new Vector3(0.26f,-8.22f,0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(4.31f,-8.22f,-5.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4f, 0), 3f, "", 0, false, false);

		save_data.active_combo = true;

		yield return null;//yield return new WaitForSeconds (1);//
	
	}

	IEnumerator InitPattern27(){

		SelectObject (0, new Vector3(-7.81f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4f, 0)*1.15f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-1.23f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 3.8f, 0)*1.15f, 3f, "", 0, false, false);

		SelectObject (0, new Vector3(2.27f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0)*1.15f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(7.01f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f,4.3f, 0)*1.15f, 3f, "", 0, false, false);

		SelectObject (0, new Vector3(-5.96f,-8.22f,0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 3.5f, 0)*1.15f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-3.42f,-8.22f,-5.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0)*1.15f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(0.26f,-8.22f,0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.3f, 0)*1.15f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(4.31f,-8.22f,-5.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4f, 0)*1.15f, 3f, "", 0, false, false);

		save_data.active_combo = true;

		yield return null;

	}

	IEnumerator InitPattern28(){


		SelectObject (2, new Vector3(-7.81f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-5.11f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(0.18f,-8.22f,0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4f, 0), 3f, "", 0, false, false);

		SelectObject (2, new Vector3(2.76f,-8.22f,5.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0), 3f, "", 0, false, false);

		SelectObject (2, new Vector3(7.93f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(-7.81f,-8.22f,0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 3.5f, 0), 3f, "", 0, false, false);


		SelectObject (2, new Vector3(7.93f,-8.22f,0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 3.5f, 0), 3f, "", 0, false, false);

		save_data.active_combo = true;

		yield return null;

	}

	// gen 2 and 3 starts here

	//Path 1(2) --  InitGen3_P1() will be calling this
	IEnumerator InitGen2_P2(){

		SelectObject (2, new Vector3(11.49f,2.79f,18.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.4f), 3f, "", 0, false, true);

		SelectObject (2, new Vector3(11.49f,-0.54f,-10.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.4f), 3f, "", 0, false, true);

		SelectObject (2, new Vector3(11.49f,-3.55f,10.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.4f), 3f, "", 0, false, true);

		yield return new WaitForSeconds (0.7f * m_TimeGap);

		SelectObject (2, new Vector3(11.49f,2.79f,18.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.4f), 3f, "", 0, false, true);

		SelectObject (2, new Vector3(11.49f,-0.54f,-10.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.4f), 3f, "", 0, false, true);

		SelectObject (2, new Vector3(11.49f,-3.55f,10.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.4f), 3f, "", 0, false, true);
		yield return null;
	
	}

	//Path 2
	IEnumerator InitGen2_P3(){


		SelectObject (0, new Vector3(-7.81f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5.25f, 0)*1.3f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-5.11f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0)*1.3f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(-2.23f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0)*1.3f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(0.18f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5.25f, 0)*1.3f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(2.76f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0)*1.3f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(5.34f,-8.22f,15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 4.5f, 0)*1.3f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(7.93f,-8.22f,-15.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 5.25f, 0)*1.3f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (1f * m_TimeGap);


		SelectObject (2, new Vector3(-5.11f,-8.22f,5.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 3.3f, 0)*1.4f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(-2.23f,-8.22f,-5.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 3.3f, 0)*1.4f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(0.18f,-8.22f,5.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 3.3f, 0)*1.4f, 3f, "", 0, false, false);


		SelectObject (0, new Vector3(2.76f,-8.22f,-5.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 3.3f, 0)*1.4f, 3f, "", 0, false, false);


		SelectObject (2, new Vector3(5.34f,-8.22f,5.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (0f, 3.3f, 0)*1.4f, 3f, "", 0, false, false);

		yield return null;
	
	}

	//Path 3
	IEnumerator InitGen2_P4(){

		SelectObject (2, new Vector3(11.49f,4.27f,0f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.2f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(19.94f,4.27f,-18.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.2f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(27.71f,4.27f,18.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.2f), 3f, "", 0, false, true);

		//------

		SelectObject (2, new Vector3(11.49f,1.49f,-10.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.2f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(14.7f,1.49f,10.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.2f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(24.05f,1.49f,0.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.2f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(27.65f,1.49f,-5.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.2f), 3f, "", 0, false, true);
		//----


		SelectObject (2, new Vector3(11.49f,-1.36f,15.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.2f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(14.6f,-1.36f,-15.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.2f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(24.05f,-1.36f,0.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.2f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(27.56f,-1.36f,5.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.2f), 3f, "", 0, false, true);

		//-----------


		SelectObject (2, new Vector3(11.49f,-4.49f,-14.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.2f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(19.37f,-4.49f,20.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.2f), 3f, "", 0, false, true);


		SelectObject (2, new Vector3(27.85f,-4.49f,-20.7f), Quaternion.Euler(0,0,0), new Vector3 (-1000f, 0, 1.2f), 3f, "", 0, false, true);

		//-------

		yield return null;//yield return new WaitForSeconds (1);

	}

	//Path 10
	IEnumerator InitGen2_P15(){

		SelectObject (2, new Vector3(-11.52f,-4.22f,10.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 4.00f, 0)*1.1f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-11.52f,-8.22f,-0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 4.00f, 0)*1.1f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-6.52f,-8.22f,-0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 5.00f, 0)*1.1f, 3f, "", 0, false, false);

		//------
		yield return new WaitForSeconds (1.7f * m_TimeGap);

		SelectObject (2, new Vector3(11.42f,-4.01f,-18.1f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, 4.00f, 0), 3f, "", 0, false, false);

		SelectObject (2, new Vector3(11.42f,-8.01f,-5.1f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, 4.00f, 0), 3f, "", 0, false, false);

		SelectObject (0, new Vector3(6.42f,-8.01f,-5.1f), Quaternion.Euler(0,0,22.45f), new Vector3 (-2, 5.00f, 0), 3f, "", 0, false, false);

		//--------
		yield return new WaitForSeconds (0.8f * m_TimeGap);

		SelectObject (0, new Vector3(0.09f,8.2f,-8.81f), Quaternion.Euler(0,0,0), new Vector3 (0, -2.5f, 0), 0.7f, "", 0, false, false);

		//-------
		yield return new WaitForSeconds (1 * m_TimeGap);

		SelectObject (2, new Vector3(-7.9f,8.2f,-8.81f), Quaternion.Euler(0,0,0), new Vector3 (0, -2.5f, 0), 1f, "", 0, false, false);

		SelectObject (2, new Vector3(-3.81f,8.2f,-8.81f), Quaternion.Euler(0,0,0), new Vector3 (0, -2.5f, 0), 1f, "", 0, false, false);

		SelectObject (2, new Vector3(4.18f,8.2f,-8.81f), Quaternion.Euler(0,0,0), new Vector3 (0, -2.5f, 0), 1f, "", 0, false, false);

		SelectObject (2, new Vector3(8.17f,8.2f,-8.81f), Quaternion.Euler(0,0,0), new Vector3 (0, -2.5f, 0), 1f, "", 0, false, false);

		yield return new WaitForSeconds (1 * m_TimeGap);

		yield return StartCoroutine (InitGen3_P16());
	}

	//Path 11(2)
	IEnumerator InitGen2_P18(){

		SelectObject (0, new Vector3(-13.32f,-4.56f,10.08f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-11.52f,-8.22f,-0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-8.17f,-9.9f,-0.9f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (1f * m_TimeGap);

		SelectObject (0, new Vector3(-13.32f,-4.56f,10.08f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-11.52f,-8.22f,-0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-8.17f,-9.9f,-0.9f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (1f * m_TimeGap);

		SelectObject (0, new Vector3(-13.32f,-4.56f,10.08f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-11.52f,-8.22f,-0.7f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-8.17f,-9.9f,-0.9f), Quaternion.Euler(0,0,-25.858f), new Vector3 (2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (2.2f * m_TimeGap);

		SelectObject (2, new Vector3(15.58f,-3.52f,-5.36f), Quaternion.Euler(0,0,55.899f), new Vector3 (-2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(14.41f,-6.83f,-5.27f), Quaternion.Euler(0,0,55.899f), new Vector3 (-2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(11.92f,-8.93f,-5.27f), Quaternion.Euler(0,0,55.899f), new Vector3 (-2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (1f * m_TimeGap);

		SelectObject (2, new Vector3(15.58f,-3.52f,-5.36f), Quaternion.Euler(0,0,55.899f), new Vector3 (-2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(14.41f,-6.83f,-5.27f), Quaternion.Euler(0,0,55.899f), new Vector3 (-2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(11.92f,-8.93f,-5.27f), Quaternion.Euler(0,0,55.899f), new Vector3 (-2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (1f * m_TimeGap);

		SelectObject (2, new Vector3(15.58f,-3.52f,-5.36f), Quaternion.Euler(0,0,55.899f), new Vector3 (-2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(14.41f,-6.83f,-5.27f), Quaternion.Euler(0,0,55.899f), new Vector3 (-2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(11.92f,-8.93f,-5.27f), Quaternion.Euler(0,0,55.899f), new Vector3 (-2, 3.750f, 0)*1.2f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (2.2f * m_TimeGap);

		SelectObject (2, new Vector3(0.11f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 1.0f, 0)*0.5f, 3f, "", 0, false, true);

		SelectObject (0, new Vector3(-0.16f,8.2f,-8.81f), Quaternion.Euler(0,0,0), new Vector3 (0, -2.5f, 0), 0.5f, "", 0, false, false);

		SelectObject (2, new Vector3(6.37f,8.2f,-8.81f), Quaternion.Euler(0,0,0), new Vector3 (0, -2.5f, 0), 0.5f, "", 0, false, false);

		SelectObject (2, new Vector3(-6.59f,8.2f,-8.81f), Quaternion.Euler(0,0,0), new Vector3 (0, -2.5f, 0), 0.5f, "", 0, false, false);

		yield return new WaitForSeconds (1.2f * m_TimeGap);
	
		yield return StartCoroutine (InitGen2_P19 ());
	}

	//Path 11(3)
	IEnumerator InitGen2_P19(){

		SelectObject (2, new Vector3(-8.37f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*0.7f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-4.38f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*0.7f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-0.28f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*0.7f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(4f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*0.7f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(4f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*0.7f, 3f, "", 0, false, false);

		//-------
		yield return new WaitForSeconds (1.3f * m_TimeGap);

		SelectObject (2, new Vector3(-4.38f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0), 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-0.28f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0), 3f, "", 0, false, false);

		SelectObject (2, new Vector3(4f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0), 3f, "", 0, false, false);
		//-----------
		yield return new WaitForSeconds (1 * m_TimeGap);

		SelectObject (2, new Vector3(-6.42f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*1.1f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-2.33f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*1.1f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(1.86f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*1.1f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(5.95f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*1.1f, 3f, "", 0, false, false);
		//--------
		yield return new WaitForSeconds (1 * m_TimeGap);

		SelectObject (2, new Vector3(-4.38f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*0.8f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-0.28f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*0.8f, 3f, "", 0, false, false);
		//-------
		SelectObject (2, new Vector3(4f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*0.8f, 3f, "", 0, false, false);

		yield return new WaitForSeconds (1.4f * m_TimeGap);

		yield return StartCoroutine (InitGen3_P20 ());
	}

	//Path 1 ( PatternsB --> Pattern_1 )
	IEnumerator InitGen3_P1(){


		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P1_L1", 1.53f, true, false);

		yield return new WaitForSeconds (0.79f * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P1_L2", 1.55f, true, false);

		yield return new WaitForSeconds (1 * m_TimeGap);

		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P1_L3", 1.28f, true, false);

		yield return new WaitForSeconds (1 * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P1_L4", 1.47f, true, false);

		yield return new WaitForSeconds (1 * m_TimeGap);

		yield return StartCoroutine (InitGen2_P2 ());
	}

	//Path 4 ( PatternsB --> Pattern_5 )
	IEnumerator InitGen3_P5(){


		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P5_L1", 2.53f, true, false);

		yield return new WaitForSeconds (0.55f * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P5_L2", 1.55f, true, false);

		yield return new WaitForSeconds (1.09f * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P5_L3", 1.49f, true, false);

		yield return new WaitForSeconds (1.09f * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P5_L4", 1.52f, true, false);

		yield return new WaitForSeconds (1.09f * m_TimeGap);

		yield return StartCoroutine (InitGen3_P6 ());
	}

	//Path 4(2) ( PatternsB --> Pattern_6 ) InitGen3_P5() will be calling this
	IEnumerator InitGen3_P6(){

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P6_L1_1", 2.33f, true, false);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P6_L1_2", 2.33f, true, false);

		yield return new WaitForSeconds (0.1f * m_TimeGap);

		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P6_L2", 3.47f, true, false);

		yield return new WaitForSeconds (2.39f * m_TimeGap);

		SelectObject (2, new Vector3(0.11f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*0.82f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(3.81f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*0.82f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-3.79f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*0.82f, 3f, "", 0, false, false);

		yield return null;//yield return new WaitForSeconds (1);
	
	}

	//Path 5 ( PatternsB --> Pattern_7 )
	IEnumerator InitGen3_P7(){


		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P7_L1", 1.80f, true, false);

		yield return new WaitForSeconds (0.1f * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P7_L2", 3.47f, true, false);

		yield return new WaitForSeconds (1.17f * m_TimeGap);

		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P7_L3", 2.87f, true, false);

		yield return new WaitForSeconds (1.08f * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P7_L4", 1.52f, true, false);

		yield return new WaitForSeconds (1.02f * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P7_L5", 1.52f, true, false);

		yield return new WaitForSeconds (1.02f * m_TimeGap);

		yield return StartCoroutine (InitGen3_P8 ());
	
	}

	//Path 5(2) ( PatternsB --> Pattern_8 ) InitGen3_P7() will be calling this
	IEnumerator InitGen3_P8(){

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P8_L1_1", 2.98f, true, false);

		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P8_L1_2", 2.98f, true, false);


		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P8_L1_3", 2.98f, true, false);

		yield return null;//yield return new WaitForSeconds (1);
	}

	//Path 6 ( PatternsB --> Pattern_9 )
	IEnumerator InitGen3_P9(){


		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P9_L1", 1.8f, true, false);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P9_L2", 1.8f, true, false);


		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P9_L3", 1.8f, true, false);

		yield return null;//yield return new WaitForSeconds (1);

	}

	//Path 7 ( PatternsB --> Pattern_11 )
	IEnumerator InitGen3_P11(){


		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P11_L1_1", 3.35f, true, false);


		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P11_L1_2", 3.35f, true, false);


		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P11_L1_3", 3.35f, true, false);

		yield return new WaitForSeconds (2.21f * m_TimeGap);


		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P11_L2_1", 2.81f, true, false);


		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P11_L2_2", 2.81f, true, false);


		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P11_L2_3", 2.81f, true, false);

		yield return null;//yield return new WaitForSeconds (1);
	
	}

	//Path 8 ( PatternsB --> Pattern_12 )
	IEnumerator InitGen3_P12(){

		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P12_L1", 3.35f, true, false);

		yield return new WaitForSeconds (1.21f * m_TimeGap);


		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P12_L2", 2.81f, true, false);

		yield return new WaitForSeconds (1.21f * m_TimeGap);


		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P12_L3", 2.82f, true, false);

		yield return new WaitForSeconds (1.29f * m_TimeGap);


		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P12_L4", 2.04f, true, false);

		yield return new WaitForSeconds (1.15f * m_TimeGap);


		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P12_L5", 1.74f, true, false);

		yield return new WaitForSeconds (1.20f * m_TimeGap);


		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P12_L6", 2.49f, true, false);

		yield return null;

	}

	//Path 9 ( PatternsA --> Pattern_13 )
	IEnumerator InitGen3_P13(){


		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P13_L1", 1.97f, true, false);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P13_L1_2", 1.97f, true, false);

		yield return new WaitForSeconds (0.91f * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P13_L2", 2.24f, true, false);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P13_L2_2", 2.24f, true, false);

		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P13_L3", 2.82f, true, false);

		yield return new WaitForSeconds (1);
	
		yield return StartCoroutine (InitGen3_P14 ());
	}

	//Path 9(2) ( PatternsA --> Pattern_14 )  InitGen3_P13() will be calling this
	IEnumerator InitGen3_P14(){

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P14_L1", 2.20f, true, false);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P14_L1_2", 2.20f, true, false);

		yield return new WaitForSeconds (0.91f * m_TimeGap);

		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P14_L2", 3.35f, true, false);

		yield return new WaitForSeconds (1.2f * m_TimeGap);

		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P14_L3", 2.30f, true, false);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P14_L3_2", 2.30f, true, false);

		yield return null;//yield return new WaitForSeconds (1);

	}

	//Path 10(2) ( PatternsA --> Pattern_16 )  InitGen2_P15() will be calling this
	IEnumerator InitGen3_P16(){

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P16_L1", 2.04f, true, false);

		yield return new WaitForSeconds (1.16f * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P16_L2", 2.19f, true, false);

		yield return new WaitForSeconds (1.28f * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P16_L3", 1.77f, true, false);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P16_L3_2", 1.77f, true, false);

		yield return new WaitForSeconds (1 * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P16_L4", 1.67f, true, false);

		yield return new WaitForSeconds (1.2f * m_TimeGap);

		SelectObject (2, new Vector3(0.11f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*1.1f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(3.81f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*1.1f, 3f, "", 0, false, false);

		SelectObject (2, new Vector3(-3.79f,-8.01f,0.42f), Quaternion.Euler(0,0,0), new Vector3 (0, 6.0f, 0)*1.1f, 3f, "", 0, false, false);

		yield return null;//yield return new WaitForSeconds (1);

	}

	//Path 11(4) ( PatternsA --> Pattern_20 )
	IEnumerator InitGen3_P20(){

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P20_L1", 4f, true, false);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P20_L1_2", 4f, true, false);

		yield return new WaitForSeconds (0.93f * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P20_L2", 2.03f, true, false);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P20_L2_2", 2.03f, true, false);

		yield return new WaitForSeconds (1.13f * m_TimeGap);

		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P20_L3", 2.03f, true, false);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P20_L3_2", 2.30f, true, false);

		yield return new WaitForSeconds (0.82f * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P20_L4", 2.04f, true, false);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P20_L4_2", 2.04f, true, false);

		yield return new WaitForSeconds (1.1f * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P20_L5", 2.21f, true, false);

		yield return new WaitForSeconds (0.1f * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P20_L6", 1.93f, true, false);

		yield return new WaitForSeconds (1);

		yield return StartCoroutine (InitGen3_P21 ());

	}

	//Path 11(5) ( PatternsA --> Pattern_21 )
	IEnumerator InitGen3_P21(){

		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P21_L1", 2.18f, true, false);

		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P21_L1_2", 2.18f, true, false);

		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P21_L1_3", 2.18f, true, false);

		yield return new WaitForSeconds (1.25f * m_TimeGap);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P21_L2", 1.55f, true, false);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P21_L2_2", 1.55f, true, false);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P21_L2_3", 1.55f, true, false);

		yield return new WaitForSeconds (1.12f * m_TimeGap);

		SelectObject (0, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P21_L3", 2.01f, true, false);

		SelectObject (2, m_DefaultInstantiatingPos, Quaternion.Euler(0,0,0), Vector3.zero, 3f, "P21_L3_2", 2.01f, true, false);

		yield return null;//yield return new WaitForSeconds (1);

	}
	// gen 2 and 3 ends here
}
