﻿using UnityEngine;
using System.Collections;

public class FooterScript : MonoBehaviour {

	public GameObject script_obj;

	public int idValu;

	public bool active = true;

	private TestItems m_FooterCollidingTestRef;

    void OnTriggerEnter(Collider other)
    {
		m_FooterCollidingTestRef = other.gameObject.GetComponentInParent<TestItems> ();

		if (!m_FooterCollidingTestRef.DidGoToGameArea)
			return;

		if (m_FooterCollidingTestRef.MyType != 0 && m_FooterCollidingTestRef.killready) {
			script_obj.GetComponent<Scare_Meter> ().WhenBadItemHitOnFooter(TrailPointScript.bGldnTm);
		}

		if(m_FooterCollidingTestRef.killready)
		{
			other.gameObject.GetComponentInParent<TestItems> ().Reset ();
		}
	}

	void OnTriggerExit(Collider other)
	{
		m_FooterCollidingTestRef = other.gameObject.GetComponentInParent<TestItems> ();

		if (!m_FooterCollidingTestRef.DidGoToGameArea)
			return;

		if (m_FooterCollidingTestRef.MyType != 0 && m_FooterCollidingTestRef.killready) {
			
			script_obj.GetComponent<Scare_Meter> ().WhenBadItemHitOnFooter(TrailPointScript.bGldnTm);
		}

		if(m_FooterCollidingTestRef.killready)
		{
			other.gameObject.GetComponentInParent<TestItems> ().Reset ();
		}
	}


   
}
